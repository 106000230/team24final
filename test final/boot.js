var bootState = {
    preload: function () {
        // Load the progress bar image.
        game.load.image('progressBar', 'assets/progressBar.png'); 
    }, 
    create: function() {
        // Set some game settings.
        //game.stage.backgroundColor = '#000000';
        game.stage.backgroundColor = 'rgb(92,148,252)'; //藍色背景
        game.physics.startSystem(Phaser.Physics.ARCADE); 
        game.renderer.renderSession.roundPixels = true; 
        console.log("in boot state");
        // Start the load state.
        game.state.start('load');
    }
}