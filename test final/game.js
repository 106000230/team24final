//Initialize Phaser
var game = new Phaser.Game(272,240,Phaser.AUTO,'canvas');
//Define our global variable
game.global = {score: 0,coin: 0, life: 3 , which_world: "1-1",timer: 300,time_up:false,who: "small"};
//Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('median', medianState);
game.state.add('play',playState);
game.state.add('map1-2', map1_2State);
game.state.add('map1-3', map1_3State);
//Start the 'boot' state
game.state.start('boot');