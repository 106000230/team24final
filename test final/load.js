var loadState = { 
    preload: function () {
        
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 100, 
        'loading...', { font: '33px VT323', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 130, 'progressBar'); progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);




        // Load all game assets
        game.load.audio('level1-1_bgm', 'assets/level1-1.ogg');
        game.load.spritesheet('smallplayer','assets/small_mario.png',16 ,16);
        game.load.spritesheet('bigplayer','assets/big_mario.png',16 ,32);
        game.load.spritesheet('fireplayer','assets/fire_mario.png',16 ,32);
        game.load.spritesheet('coin', 'assets/coin.png',16,16);
        game.load.spritesheet('1-2goomba', 'assets/goomba_blue.png',16,16);
        game.load.spritesheet('goomba', 'assets/goomba.png',16,16);
        game.load.spritesheet('koopa', 'assets/enemy.png',16,32);
        game.load.spritesheet('flykoopa', 'assets/enemy.png',16,32);
        game.load.spritesheet('star', 'assets/star.png',16,16);
        game.load.spritesheet('weapon', 'assets/fireBall.png',16,16);
        game.load.image('tileset', 'assets/tileset.png');
        game.load.image('tileset2', 'assets/tileset2.png');
        game.load.image('tileset3', 'assets/tileset3.png');
        game.load.image('coin2', 'assets/coin2.png');
        game.load.image('UI_coin', 'assets/UI_coin.png');
        game.load.spritesheet('question', 'assets/question.png',16,16);
        game.load.spritesheet('under_pipe_question', 'assets/under_pipe_question.png',16,16);
        game.load.image('brick', 'assets/brick.png');
        game.load.image('under_pipe_brick', 'assets/under_pipe_brick.png');
        game.load.image('1-2lifebrick', 'assets/under_pipe_lifebrick.png');
        game.load.image('lifebrick', 'assets/lifebrick.png');
        game.load.image('fix', 'assets/fix.png');
        game.load.image('bar', 'assets/bar.png');
        game.load.image('1-2bar', 'assets/1-2bar.png');
        game.load.tilemap('map', 'assets/map.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('1-2map', 'assets/1-2map.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('1-3map', 'assets/1-3map.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.audio('eat_coin_sound', 'assets/Coin.wav');
        game.load.audio('goomba_die', 'assets/goomba_die.wav');
        game.load.audio('player_die', 'assets/playerDie.wav');
        game.load.audio('go_into_pipe', 'assets/go_into_pipe.wav');
        game.load.audio('level1_2&under_pipe', 'assets/level1_2&under_pipe.ogg');
        game.load.audio('Item', 'assets/Item.wav');
        game.load.audio('star_power', 'assets/star_power.ogg');
        game.load.audio('1up', 'assets/1up.wav');
        game.load.audio('Powerup', 'assets/Powerup.wav');
        game.load.audio('Kick', 'assets/Kick.wav');
        game.load.audio('breakbrick', 'assets/brick.wav');
        game.load.audio('fireball', 'assets/fireball.wav');
        game.load.audio('jumpBig', 'assets/jumpBig.wav');
        game.load.audio('jumpSmall', 'assets/jumpSmall.wav');
        game.load.audio('run_out_of_time', 'assets/runningOutOfTime.wav');
        game.load.audio('game_over', 'assets/gameover.wav');
        game.load.image('1up_mushroom', 'assets/1up_mushroom.png');
        game.load.image('red_mushroom', 'assets/mushroom.png');
        game.load.spritesheet('flower', 'assets/flower.png',16,16);
        game.load.bitmapFont('carrier_command', 'assets/carrier_command.png', 'assets/carrier_command.xml');
        game.load.image('title', 'assets/title.png');
        game.load.image('cursor', 'assets/cursor.png');
        game.load.image('menu_bg', 'assets/menu_bg.png');
        game.load.image('mario_UI', 'assets/mario_UI.png');
        game.load.spritesheet('small_mario_flag', 'assets/mario_small_finishLevel.png',16,16);
        game.load.spritesheet('big_mario_flag', 'assets/mario_big_finishLevel.png',16,32);
        game.load.spritesheet('fire_mario_flag', 'assets/mario_fire_finishLevel.png',16,32);
        game.load.audio('flagpole', 'assets/flagpole.wav');
        game.load.audio('level_clear', 'assets/level_clear.wav');
        game.load.image('flag', 'assets/flag.png');
        // Load a new asset that we will use in the menu state
    },
    create: function() {
        // Go to the menu state 
        console.log("in load state");
        game.state.start('menu'); 
    }
}
            