var medianState = { 
    create: function() {
        //game.global.timer = 120;
        /*console.log("in median state");
        console.log("score = " + game.global.score);
        console.log("coin = " + game.global.coin);
        console.log("life = " + game.global.life);
        console.log("which_world = "+ game.global.which_world);
        console.log("timer = " + game.global.timer );
        //console.log("first_enter_1_1 =" +game.global.first_enter_1_1);
        console.log("time_up = " + game.global.time_up);*/
        game.stage.backgroundColor = 'rgb(0,0,0)';





        //UI
        MARIO_text = game.add.text(18, 10,"MARIO", { font: '22px VT323', fill: '#ffffff' });
        MARIO_text.scale.y = 0.6;
        MARIO_text.fixedToCamera = true;
        //MARIO_text.cameraOffset.setTo(18, 10);
        WORLD_text = game.add.text(150, 10,"WORLD", { font: '22px VT323', fill: '#ffffff' });
        WORLD_text.scale.y = 0.6;
        WORLD_text.fixedToCamera = true;
        TIME_text = game.add.text(220, 10,"TIME", { font: '22px VT323', fill: '#ffffff' });
        TIME_text.scale.y = 0.6;
        TIME_text.fixedToCamera = true;
        if(game.global.score == 0)
            this.score_text = game.add.text(18, 20,"000000", { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.score < 1000)
            this.score_text = game.add.text(18, 20,"000"+game.global.score, { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.score < 10000 && game.global.score >= 1000 )
            this.score_text = game.add.text(18, 20,"00"+ game.global.score, { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.score < 100000 && game.global.score >= 10000 )
            this.score_text = game.add.text(18, 20,"0"+ game.global.score, { font: '22px VT323', fill: '#ffffff' }); 
        else
            this.score_text = game.add.text(18, 20,game.global.score, { font: '22px VT323', fill: '#ffffff' });
        this.score_text.scale.y = 0.6;
        this.score_text.fixedToCamera = true;
        this.world_text = game.add.text(160, 20,game.global.which_world, { font: '22px VT323', fill: '#ffffff' });
        this.world_text.scale.y = 0.6;
        this.world_text.fixedToCamera = true;
        //this.time_text = game.add.text(225, 20,game.global.timer, { font: '22px VT323', fill: '#ffffff' });
        //this.time_text.scale.y = 0.6;
        //this.time_text.fixedToCamera = true;
        UI_coin = game.add.sprite(90, 20, 'UI_coin');
        UI_coin.scale.setTo(0.8,0.8);
        UI_coin.fixedToCamera = true;
        if(game.global.coin == 0)
            this.money_text = game.add.text(102, 20,"x00", { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.coin < 10)
            this.money_text = game.add.text(102, 20,"x0"+game.global.coin, { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.coin >= 10)
            this.money_text = game.add.text(102, 20,game.global.coin, { font: '22px VT323', fill: '#ffffff' });
        //this.money_text = game.add.text(102, 20,"x0"+game.global.coin , { font: '22px VT323', fill: '#ffffff' });
        this.money_text.scale.y = 0.6;
        this.money_text.fixedToCamera = true;

        this.game_over_sound = game.add.audio('game_over'); 

        

        

     
        
        if(game.global.time_up == true)
        {
            //this.game_over_sound.play();
            this.text = game.add.text(136, 120,"TIME UP", { font: '22px VT323', fill: '#ffffff' });
            this.text.anchor.setTo(0.5,0.5);
            game.time.events.add(
                3000, // Start callback after 1000ms
                function(){
                    console.log("time_up")
                    game.global.time_up = false;
                    game.state.start('median');

                },
                this
            );
        }
        else if(game.global.life > 0){
            
            this.text = game.add.text(136, 80,"WORLD  " + game.global.which_world, { font: '25px VT323', fill: '#ffffff' });
            this.text.anchor.setTo(0.5,0.5);
            this.text.scale.y = 0.5;
            this.mario_UI = game.add.sprite(120, 110, 'mario_UI');
            this.mario_UI.anchor.setTo(0.5,0.5);
            this.life_text = game.add.text(130, 105,"x "+game.global.life , { font: '22px VT323', fill: '#ffffff' });
            this.life_text.scale.y = 0.5;
            game.time.events.add(
                3000, // Start callback after 1000ms
                function(){
                    if(game.global.which_world == "1-1")
                        game.state.start('play');
                    else if(game.global.which_world == "1-2")
                        game.state.start('map1-2');
                    else if(game.global.which_world == "1-3")
                        game.state.start('map1-3');
                },
                this
            );
            
        }
        else if(game.global.life == 0){
            this.game_over_sound.play();
            this.text = game.add.text(136, 120,"GAME OVER", { font: '25px VT323', fill: '#ffffff' });
            this.text.anchor.setTo(0.5,0.5);
            this.text.scale.y = 0.5;
            game.time.events.add(
                4000, // Start callback after 1000ms
                function(){
                    game.state.start('menu'); 
                },
                this
            );
        }

        
    }, 
    start: function() {
        // Start the actual game game.state.start('play'); 
    }, 
}; 
  