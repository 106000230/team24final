var menuState = { 
    create: function() {
        console.log("in menu state");
        bg = game.add.sprite(0, 0, 'menu_bg');

        title = game.add.sprite(136, 125, 'title');
        title.anchor.setTo(0.5,0.5);
        

        game.global.score = 0;
        game.global.coin = 0;
        game.global.life = 3;
        game.global.which_world = "1-1";
        game.global.timer = 120;
        game.global.time_up = false;
        game.global.who = "small";

        /*console.log("score = " + game.global.score);
        console.log("coin = " + game.global.coin);
        console.log("life = " + game.global.life);
        console.log("which_world = "+ game.global.which_world);
        console.log("timer = " + game.global.timer );
        //console.log("first_enter_1_1 =" +game.global.first_enter_1_1);
        console.log("time_up = " + game.global.time_up);*/

        //UI
        MARIO_text = game.add.text(18, 10,"MARIO", { font: '22px VT323', fill: '#ffffff' });
        MARIO_text.scale.y = 0.6;
        MARIO_text.fixedToCamera = true;
        //MARIO_text.cameraOffset.setTo(18, 10);
        WORLD_text = game.add.text(150, 10,"WORLD", { font: '22px VT323', fill: '#ffffff' });
        WORLD_text.scale.y = 0.6;
        WORLD_text.fixedToCamera = true;
        TIME_text = game.add.text(220, 10,"TIME", { font: '22px VT323', fill: '#ffffff' });
        TIME_text.scale.y = 0.6;
        TIME_text.fixedToCamera = true;
        this.score_text = game.add.text(18, 20,"00000"+game.global.score, { font: '22px VT323', fill: '#ffffff' });
        this.score_text.scale.y = 0.6;
        this.score_text.fixedToCamera = true;
        this.world_text = game.add.text(160, 20,game.global.which_world, { font: '22px VT323', fill: '#ffffff' });
        this.world_text.scale.y = 0.6;
        this.world_text.fixedToCamera = true;
        //this.time_text = game.add.text(225, 20,game.global.timer, { font: '22px VT323', fill: '#ffffff' });
        //this.time_text.scale.y = 0.6;
        //this.time_text.fixedToCamera = true;
        UI_coin = game.add.sprite(90, 20, 'UI_coin');
        UI_coin.scale.setTo(0.8,0.8);
        UI_coin.fixedToCamera = true;
        this.money_text = game.add.text(102, 20,"x0"+game.global.coin , { font: '22px VT323', fill: '#ffffff' });
        this.money_text.scale.y = 0.6;
        this.money_text.fixedToCamera = true;

        this.world_1_1 = game.add.text(136,150,"World 1-1" , { font: '18px VT323', fill: '#ffffff' });
        this.world_1_1.anchor.setTo(0.5,0.5);
        this.world_1_2 = game.add.text(136,170,"World 1-2" , { font: '18px VT323', fill: '#ffffff' });
        this.world_1_2.anchor.setTo(0.5,0.5);
        this.world_1_3 = game.add.text(136,190,"World 1-3" , { font: '18px VT323', fill: '#ffffff' });
        this.world_1_3.anchor.setTo(0.5,0.5);

        this.chooser = game.add.sprite(90, 147, 'cursor');
        this.chooser.anchor.setTo(0.5,0.5);

      

        this.enter_key = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.cursors = game.input.keyboard.createCursorKeys();

        this.press = 0;

        //game.state.start('play'); 
       
    }, 
    update: function() {


        if(this.cursors.down.isDown &&　this.press == 0)
        {
            this.press = 1;
            if(this.chooser.y == 147)
            {
                this.chooser.y = 167;
            }
            else if(this.chooser.y == 167)
            {
                this.chooser.y = 187;
            }
            else    
            {
                this.chooser.y = 147;
            }
        }
        
        if(!this.cursors.down.isDown)
            this.press = 0;


        if(this.enter_key.isDown)
        {
            
            if(this.chooser.y == 147)
            {
                console.log("enter 1-1");
                game.global.which_world = "1-1";
                game.state.start('median'); 
            }
            else if(this.chooser.y == 167)
            {
                console.log("enter 1-2");
                game.global.which_world = "1-2";
                game.state.start('median');
            }
            else if(this.chooser.y == 187)
            {
                console.log("enter 1-3");
                game.global.which_world = "1-3";
                game.state.start('median'); 
            }

        }

        
        
       
    }, 
}; 
  