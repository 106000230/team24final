//Create the States
var playState = {
    preload: function(){
       

    },
    create: function(){
        
    
        //level 1-1 bgm
        this.level1_1_bgm = game.add.audio('level1-1_bgm');
        this.level1_1_bgm.loop = true;
        this.level1_1_bgm.play();
       
    
        //game.renderer.renderSession.roundPixels = true;
        //game.physics.startSystem(Phaser.Physics.ARCADE);

        game.stage.backgroundColor = 'rgb(92,148,252)'; //藍色背景

        this.map = game.add.tilemap('map'); 

        this.map.addTilesetImage('tileset');
        this.map.addTilesetImage('tileset2');
        
        

        this.map.setCollision(17);
        this.map.setCollisionBetween(14,16);
        this.map.setCollisionBetween(21, 22,true);
        this.map.setCollisionBetween(27, 28,true);
        this.map.setCollision(40);
        this.map.setCollision(41);
        this.map.setCollision(11);
        //this.map.setCollision(102);
        this.map.setCollision(103);
        this.map.setCollision(105);
        this.map.setCollisionBetween(279,281);
        this.map.setCollisionBetween(306,310);
    
    
        this.backgroundlayer = this.map.createLayer('background');
        
        this.player = game.add.sprite(32, 150, 'smallplayer');
        this.bigplayer = game.add.sprite(32, 150, 'bigplayer');
        this.fireplayer = game.add.sprite(32, 150, 'fireplayer');
    
        this.green_mushroom = game.add.sprite(-20, -20, '1up_mushroom');
        this.red_mushroom = game.add.sprite(-20, -20, 'red_mushroom');
        this.flower = game.add.sprite(-20, -20, 'flower');
        this.flower.frame = 0;
        this.flower.animations.add('anim', [0, 1, 2, 3], 10, true);

        


        this.star = game.add.sprite(-20, -20, 'star');
        this.star.frame = 0;
        this.star.animations.add('anim', [0, 1, 2, 3], 10, true);
        game.physics.enable(this.star);
        //this.player.scale.setTo(0.5,0.5);
        game.physics.enable(this.player);
        game.physics.enable(this.bigplayer);
        game.physics.enable(this.fireplayer);

        
        


        this.player.animations.add('rightwalk', [1, 3, 4], 10, true);
        this.player.animations.add('leftwalk', [11,9,8], 10, true);
        this.player.animations.add('rightjump', [5], 8, true);
        this.player.animations.add('leftjump', [7], 8, true);
        this.player.animations.add('flag', [0,1], 8, true);

        this.bigplayer.animations.add('rightwalk', [3, 4,2], 10, true);
        this.bigplayer.animations.add('leftwalk', [9,8,10], 10, true);
        this.bigplayer.animations.add('rightjump', [5], 8, true);
        this.bigplayer.animations.add('leftjump', [7], 8, true);
        //this.bigplayer.animations.add('flag', [0,1], 8, true);

        this.fireplayer.animations.add('rightwalk', [ 3, 4,2], 10, true);
        this.fireplayer.animations.add('leftwalk', [9,8,10], 10, true);
        this.fireplayer.animations.add('rightjump', [5], 8, true);
        this.fireplayer.animations.add('leftjump', [7], 8, true);
        //this.fireplayer.animations.add('flag', [0,1], 8, true);

        //this.fireplayer.animations.add('firerightwalk', [13, 14, 15], 10, true);
        //this.fireplayer.animations.add('fireleftwalk', [22,21,20], 10, true);
        



        this.worldlayer = this.map.createLayer('world');
        

        this.backgroundlayer.resizeWorld();
        this.worldlayer.resizeWorld();
        

        //goomba敵人
        this.goombas = game.add.group();
        this.goombas.enableBody = true;
        this.map.createFromObjects('goomba', 11,"goomba",0,true,false,this.goombas) ;
        this.goombas.setAll('body.immovable', true);
        this.goombas.callAll('animations.add', 'animations', 'walk', [0, 1], 4, true);
        this.goombas.callAll('animations.play', 'animations', 'walk');
        this.goombas.setAll('checkWorldBounds', true);
        this.goombas.setAll('outOfBoundsKill', true);
        this.goombas.setAll('body.velocity.x', -20);
        this.goombas.setAll('body.gravity.y', 300);
        this.goombas.setAll('body.moves', false);

        //koopa敵人
        this.koopas = game.add.group();
        this.koopas.enableBody = true;
        this.map.createFromObjects('koopa', 1,"koopa",0,true,false,this.koopas) ;
        this.koopas.setAll('body.immovable', true);
        this.koopas.setAll('body.immovable', true);
        this.koopas.callAll('animations.add', 'animations', 'walk', [6, 7], 4, true);
        this.koopas.callAll('animations.add', 'animations', 'squish', [10, 11], 8, true);
        this.koopas.callAll('animations.play', 'animations', 'walk');
        this.koopas.setAll('checkWorldBounds', true);
        this.koopas.setAll('outOfBoundsKill', true);
        this.koopas.setAll('body.velocity.x', 0);
        this.koopas.setAll('body.gravity.y', 0);
        //this.goombas.setAll('body.moves', false);
        
        
        
        
     

        this.cursors = game.input.keyboard.createCursorKeys();

        
        //普通沒東西的brick
        this.bricks = game.add.group()
        this.bricks.enableBody = true;
        this.map.createFromObjects('brick', 15,"brick",0,true,false,this.bricks) ;
        this.bricks.setAll('body.immovable', true);

        /*//撞了會吃金幣的brick
        this.coin_bricks = game.add.group()
        this.coin_bricks.enableBody = true;
        this.map.createFromObjects('coin_brick', 14,"question",0,true,false,this.coin_bricks) ;
        this.coin_bricks.setAll('body.immovable', true);
        this.coin_bricks.callAll('animations.add', 'animations', 'spin', [0, 1, 2], 4, true);
        this.coin_bricks.callAll('animations.play', 'animations', 'spin');*/

        

        //撞了會有菇菇或花花的brick
        this.mushroom_and_flowerbricks = game.add.group()
        this.mushroom_and_flowerbricks.enableBody = true;
        this.map.createFromObjects('mushroom&flower', 14,"question",0,true,false,this.mushroom_and_flowerbricks) ;
        this.mushroom_and_flowerbricks.setAll('body.immovable', true);
        this.mushroom_and_flowerbricks.callAll('animations.add', 'animations', 'spin', [0, 1, 2], 4, true); //問號磚塊的動畫
        this.mushroom_and_flowerbricks.callAll('animations.play', 'animations', 'spin');
        
        
        //隱藏的brick 撞了會有綠菇菇
        this.lifebricks = game.add.group()
        this.lifebricks.enableBody = true;
        this.map.createFromObjects('life_brick', 1,"lifebrick",0,true,false,this.lifebricks) ;
        this.lifebricks.setAll('body.immovable', true);
        this.lifebricks.setAll('body.checkCollision.up', false);
       
        //撞了會有星星的brick
        this.starbricks = game.add.group()
        this.starbricks.enableBody = true;
        this.map.createFromObjects('star_brick', 15,"brick",0,true,false,this.starbricks) ;
        this.starbricks.setAll('body.immovable', true);

        //可以連續撞得金幣的brick
        this.continue_coinbricks = game.add.group()
        this.continue_coinbricks.enableBody = true;
        this.map.createFromObjects('continue_coin', 15,"brick",0,true,false,this.continue_coinbricks) ;
        this.continue_coinbricks.setAll('body.immovable', true);


        //撞了會吃金幣的brick
        this.coin_bricks = game.add.group()
        this.coin_bricks.enableBody = true;
        this.map.createFromObjects('coin_brick', 1,"question",0,true,false,this.coin_bricks) ;
        this.coin_bricks.setAll('body.immovable', true);
        this.coin_bricks.callAll('animations.add', 'animations', 'spin', [0, 1, 2], 4, true);
        this.coin_bricks.callAll('animations.play', 'animations', 'spin');

        //水管底下的出入口們
        this.pipe_entrys = game.add.group();
        this.pipe_entrys.enableBody = true;
        this.map.createFromObjects('pipe_entry', 1,"",0,true,false,this.pipe_entrys) ;
        this.pipe_exits = game.add.group();
        this.pipe_exits.enableBody = true;
        this.map.createFromObjects('pipe_exit', 1,"",0,true,false,this.pipe_exits) ;
        this.under_pipe_exits = game.add.group();
        this.under_pipe_exits.enableBody = true;
        this.map.createFromObjects('under_pipe_exit', 1,"",0,true,false,this.under_pipe_exits) ;
        this.under_pipe_entrys = game.add.group();
        this.under_pipe_entrys.enableBody = true;
        this.map.createFromObjects('under_pipe_entry', 1,"",0,true,false,this.under_pipe_entrys) ;
        this.under_pipe_entrys.forEachAlive(function(under_pipe_entry){
            console.log("under_pipe_entry.x = " + under_pipe_entry.x + "under_pipe_entry.y = " + under_pipe_entry.y )
            this.under_pipe_entry_pos_x = under_pipe_entry.x;
            this.under_pipe_entry_pos_y = under_pipe_entry.y;
        }, this, null);
        this.pipe_exits.forEachAlive(function(pipe_exit){
            console.log("under_pipe_entry.x = " + pipe_exit.x + "under_pipe_entry.y = " + pipe_exit.y )
            this.pipe_exit_pos_x = pipe_exit.x;
            this.pipe_exit_pos_y = pipe_exit.y;
        }, this, null);
        //水管底下的金幣
        //撞了會吃金幣的brick
        this.under_pipe_coins = game.add.group()
        this.under_pipe_coins.enableBody = true;
        this.map.createFromObjects('under_pipe_coin', 155,"coin2",0,true,false,this.under_pipe_coins) ;
        this.coin_bricks.setAll('body.immovable', true);
         

        //this.goombas.setAll('body.velocity.x', -20);
        //this.goombas.setAll('body.gravity.y', 300);
       
        //吃金幣的聲音
        this.eat_coin_sound = game.add.audio('eat_coin_sound');
        //goomba死掉的聲音
        this.goomba_die = game.add.audio('goomba_die');
        //player死掉的聲音
        this.player_die_sound = game.add.audio('player_die');
        //player進入pipe的聲音 & degrade的聲音
        this.go_into_pipe_sound = game.add.audio('go_into_pipe');
        //在水管下的bgm
        this.under_pipe_bgm = game.add.audio('level1_2&under_pipe');
        //菇菇/花花出來的聲音
        this.item_sound = game.add.audio('Item');
        //life+1的聲音
        this.add_life_sound = game.add.audio('1up');
        //吃到星星、菇菇、花花的聲音
        this.Powerup_sound= game.add.audio('Powerup');
        this.star_power_sound = game.add.audio('star_power');
        //踢走敵人聲音
        this.kick_sound = game.add.audio('Kick');
        //撞碎磚塊的聲音
        this.brick_sound = game.add.audio('breakbrick');
        //射火球的聲音
        this.shoot_sound = game.add.audio('fireball');
        //小人跳躍的聲音
        this.jumpSmall_sound = game.add.audio('jumpSmall');
        //大人&射擊人跳躍的聲音
        this.jumpBig_sound = game.add.audio('jumpBig');
        //剩下100秒的聲音
        this.run_out_of_time_sound = game.add.audio("run_out_of_time");
        //滑下旗子的聲音
        this.flagpole_sound = game.add.audio("flagpole");
        //level clear
        this.level_clear = game.add.audio("level_clear");
        

        this.playerdirection = "right";
        this.playeridlecounter = 0;
        this.iskillinggoomba = 0;
        this.playerDie = 0;

        this.playerOnbrick = 0;

        
        this.under_pipe_exit_counter = 0;

        //this.player.x = 100;
        //this.bigplayer.x = 100;
        //this.fireplayer.x = 100;
        this.koopa_squished = 0;
        this.koopa_kicked = 0;

        this.continue_coin_count = 8;
        this.star_power = 0;

        this.orig_tint = this.player.tint
        this.star_tint = 0.5 * 0xffffff;
        //this.player.tint = this.star_tint;

        
        
        this.emitter = game.add.emitter(0, 0, 1);
        this.emitter.makeParticles('brick');
        this.emitter.setYSpeed(70); 
        this.emitter.setXSpeed(-70);
        this.emitter.setScale(0.5, 0, 0.5, 0, 400);
        this.emitter.gravity = 0;
        this.emitter2 = game.add.emitter(0, 0, 1);
        this.emitter2.makeParticles('brick');
        this.emitter2.setYSpeed(70); 
        this.emitter2.setXSpeed(70);
        this.emitter2.setScale(0.5, 0, 0.5, 0, 400);
        this.emitter2.gravity = 0;
        this.emitter3 = game.add.emitter(0, 0, 1);
        this.emitter3.makeParticles('brick');
        this.emitter3.setYSpeed(-70); 
        this.emitter3.setXSpeed(70);
        this.emitter3.setScale(0.5, 0, 0.5, 0, 400);
        this.emitter3.gravity = 0;
        this.emitter4 = game.add.emitter(0, 0, 1);
        this.emitter4.makeParticles('brick');
        this.emitter4.setYSpeed(-70); 
        this.emitter4.setXSpeed(-70);
        this.emitter4.setScale(0.5, 0, 0.5, 0, 400);
        this.emitter4.gravity = 0;

        this.coin_bricks = game.add.group()
        this.coin_bricks.enableBody = true;
        this.map.createFromObjects('coin_brick', 1,"question",0,true,false,this.coin_bricks) ;
        this.coin_bricks.setAll('body.immovable', true);
        this.coin_bricks.callAll('animations.add', 'animations', 'spin', [0, 1, 2], 4, true);
        this.coin_bricks.callAll('animations.play', 'animations', 'spin');

        this.weapons = game.add.group(); // Create group
        this.weapons.enableBody = true;
        this.weapons.createMultiple(100, 'weapon',0,false);
        /*game.add.sprite(-20, -20, 'weapon', 0, this.weapons); // Add wall
        game.add.sprite(-20, -20, 'weapon', 0, this.weapons); // Add wall
        game.add.sprite(-20, -20, 'weapon', 0, this.weapons); // Add wall
        game.add.sprite(-20, -20, 'weapon', 0, this.weapons); // Add wall
        game.add.sprite(-20, -20, 'weapon', 0, this.weapons); // Add wall*/
        this.weapons.callAll('animations.add', 'animations', 'spin', [0, 1, 2, 3], 4, true);
        this.weapons.callAll('animations.add', 'animations', 'hit', [5,6], 10, false);
        this.weapons.setAll('checkWorldBounds', true);
        this.weapons.setAll('outOfBoundsKill', true);
        
        this.space_key = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        game.global.which_world = "1-1"

        //UI
        MARIO_text = game.add.text(18, 10,"MARIO", { font: '22px VT323', fill: '#ffffff' });
        MARIO_text.scale.y = 0.6;
        MARIO_text.fixedToCamera = true;
        //MARIO_text.cameraOffset.setTo(18, 10);
        WORLD_text = game.add.text(150, 10,"WORLD", { font: '22px VT323', fill: '#ffffff' });
        WORLD_text.scale.y = 0.6;
        WORLD_text.fixedToCamera = true;
        TIME_text = game.add.text(220, 10,"TIME", { font: '22px VT323', fill: '#ffffff' });
        TIME_text.scale.y = 0.6;
        TIME_text.fixedToCamera = true;
        if(game.global.score == 0)
            this.score_text = game.add.text(18, 20,"000000", { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.score < 1000)
            this.score_text = game.add.text(18, 20,"000"+game.global.score, { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.score < 10000 && game.global.score >= 1000 )
            this.score_text = game.add.text(18, 20,"00"+ game.global.score, { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.score < 100000 && game.global.score >= 10000 )
            this.score_text = game.add.text(18, 20,"0"+ game.global.score, { font: '22px VT323', fill: '#ffffff' }); 
        else
            this.score_text = game.add.text(18, 20,game.global.score, { font: '22px VT323', fill: '#ffffff' });
        
        this.score_text.scale.y = 0.6;
        this.score_text.fixedToCamera = true;
        this.world_text = game.add.text(160, 20,game.global.which_world, { font: '22px VT323', fill: '#ffffff' });
        this.world_text.scale.y = 0.6;
        this.world_text.fixedToCamera = true;
        this.time_text = game.add.text(225, 20,game.global.time, { font: '22px VT323', fill: '#ffffff' });
        this.time_text.scale.y = 0.6;
        this.time_text.fixedToCamera = true;
        UI_coin = game.add.sprite(90, 20, 'UI_coin');
        UI_coin.scale.setTo(0.8,0.8);
        UI_coin.fixedToCamera = true;
        if(game.global.coin == 0)
            this.money_text = game.add.text(102, 20,"x00", { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.coin < 10)
            this.money_text = game.add.text(102, 20,"x0"+game.global.coin, { font: '22px VT323', fill: '#ffffff' });
        else if(game.global.coin >= 10)
            this.money_text = game.add.text(102, 20,game.global.coin, { font: '22px VT323', fill: '#ffffff' });
        
        this.money_text.scale.y = 0.6;
        this.money_text.fixedToCamera = true;

        //if(game.global.first_enter_1_1 == true){
            //game.global.first_enter_1_1 = false;
            //game.global.timer = 180;
        //}

        game.time.events.loop(1000, 
            function(){
                if(game.global.timer == 50 ){
                    this.level1_1_bgm.pause();
                    this.run_out_of_time_sound.play();
                    game.time.events.add(
                        2000, // Start callback after 1000ms
                        function(){
                            this.level1_1_bgm.resume();
                        },
                        this
                    );

                }
                if(game.global.timer > 0 && !this.reachFlagpole){

                    game.global.timer-=1;
                    if(game.global.timer < 100)
                        this.time_text.setText("0"+game.global.timer);
                    else if(game.global.timer < 10)
                        this.time_text.setText("00"+game.global.timer);
                    else
                        this.time_text.setText(game.global.timer);
                }
                else if(game.global.timer == 0 && !this.playerDie)
                {
                    if(this.who == "small"){
                        this.player.body.moves = false;
                        this.playerDie = 1;
                        console.log("時間到");
                        this.player_die_sound.play();
                        this.player.animations.stop();
                        this.level1_1_bgm.pause();
                        this.player.frame = 6;
                        this.player.body.enable = false;
                        game.add.tween(this.player).to({x:this.player.x,y:this.player.y-30},300).to({x:this.player.x,y:250},800).start();
                        game.time.events.add(
                            2000, // Start callback after 1000ms
                            function(){
                                game.global.life -= 1;
                                game.global.time_up = true;
                                game.global.who = "small";
                                game.state.start('median');
                            },
                            this
                        );

                    }
                    else if(this.who == "big" || this.who == "fire"){
                        
                        this.playerDie = 1;
                        this.player_die_sound.play();
                        this.level1_1_bgm.pause();
                        if(this.who == "big"){
                            this.bigplayer.body.enable = false;
                            game.add.tween(this.bigplayer).to({x:this.bigplayer.x,y:this.bigplayer.y-30},300).to({x:this.bigplayer.x,y:250},800).start();
                            game.time.events.add(
                                2000, // Start callback after 1000ms
                                function(){
                                    game.global.life -= 1;
                                    game.global.time_up = true;
                                    game.global.who = "small";
                                    game.state.start('median');
                                },
                                this
                            );
                        }
                        else if(this.who == "fire"){
                            this.fireplayer.body.enable = false;
                            game.add.tween(this.fireplayer).to({x:this.fireplayer.x,y:this.fireplayer.y-30},300).to({x:this.fireplayer.x,y:250},800).start();
                            game.time.events.add(
                                2000, // Start callback after 1000ms
                                function(){
                                    game.global.life -= 1;
                                    game.global.time_up = true;
                                    game.global.who = "small";
                                    game.state.start('median');
                                },
                                this
                            );
                        }
                        
                    }
                }

            }, this);


        this.flagpoles = game.add.group();
        this.flagpoles.enableBody = true;
        this.map.createFromObjects('flagpole', 1,"",0,true,false,this.flagpoles) ;


        this.reachFlagpole = 0;

        this.flag = game.add.sprite(3168, 57, 'flag');
        this.flag.anchor.setTo(0.5,0.5);
        this.flag.scale.setTo(0.5,0.5);
        
        this.who = game.global.who;
        //this.who = 'small';
        this.tween_ct_brick = 0;
        this.alpha_state = 0;

        //this.player.body.linearDamping = 1;
        //this.player.body.collideWorldBounds = true;
        this.player.body.gravity.y = 300;
        this.bigplayer.body.gravity.y = 300;
        this.fireplayer.body.gravity.y = 300;
        //this.bigplayer.body.gravity.y = 300;
        //this.fireplayer.body.gravity.y = 300;
        if(this.who == "small")
            game.camera.follow(this.player); //讓相機跟著player走
        else if(this.who == "big")
            game.camera.follow(this.bigplayer); //讓相機跟著player走
        else if(this.who == "fire")
            game.camera.follow(this.fireplayer); //讓相機跟著player走

        this.player.x = 150;
        this.bigplayer.x = 150;
        this.fireplayer.x = 150;

        game.global.timer = 180;
     },
    update: function(){


        this.weapons.forEachExists(function(weapon){
            if(weapon.body.velocity.x == 0)
                weapon.kill();
        }, this, null);

        if(this.who == 'small')
        {
            
            this.bigplayer.body.enable = false;
            this.bigplayer.alpha = 0;
            this.fireplayer.body.enable = false;
            this.fireplayer.alpha = 0;
        }
        else if(this.who == 'big')
        {
            this.player.body.enable = false;
            this.player.alpha = 0;
            this.fireplayer.body.enable = false;
            this.fireplayer.alpha = 0;
        }
        else if(this.who == 'fire')
        {
            this.player.body.enable = false;
            this.player.alpha = 0;
            this.bigplayer.body.enable= false;
            this.bigplayer.alpha = 0;
        }

        this.playerOnbrick = 0;
        this.goombas.forEachAlive(function(goomba){
            if(goomba.inCamera)
            {
                if(!this.iskillinggoomba)
                    goomba.body.moves = true;
                goomba.body.gravity.y = 300;
                //為了讓goomba撞到水管會反彈
                velocity = [20,-20];
                if(goomba.body.velocity.x == 0){
                    goomba.body.velocity.x = random_velocity(velocity);
                }   
            }
        }, this, null);
        this.koopas.forEachAlive(function(koopa){
            if(koopa.inCamera)
            {
                if(!this.koopa_squished){
                koopa.body.gravity.y = 300;
                koopa.body.velocity.x = -20;
                }

                if(this.koopa_kicked)
                {
                    console.log("hi");
                    if(koopa.body.velocity.x == 0)
                        koopa.kill();
                }
                 
            }
        }, this, null);
        

        game.physics.arcade.collide(this.player, this.continue_coinbricks,this.hitctbrick,null,this);
        game.physics.arcade.collide(this.player, this.starbricks,this.hitstarbrick,null,this);
        game.physics.arcade.collide(this.player, this.lifebricks,this.hitlifebrick,null,this);
        game.physics.arcade.collide(this.player, this.mushroom_and_flowerbricks,this.hitmfbrick,null,this);
        game.physics.arcade.collide(this.player, this.bricks,this.hitbrick,null,this);
        game.physics.arcade.collide(this.player, this.coin_bricks,this.hitcoinbrick,null,this);
        game.physics.arcade.collide(this.player, this.worldlayer);
        game.physics.arcade.overlap(this.player, this.pipe_entrys,this.at_pipe_entry,null,this);
        game.physics.arcade.overlap(this.player, this.under_pipe_exits,this.at_under_pipe_exit,null,this);
        game.physics.arcade.overlap(this.player, this.under_pipe_coins,this.eat_under_pipe_coin,null,this);
        game.physics.arcade.overlap(this.player, this.green_mushroom,this.eat_green_mushroom,null,this);
        game.physics.arcade.overlap(this.player, this.red_mushroom,this.eat_red_mushroom,null,this);
        game.physics.arcade.overlap(this.player, this.flower,this.eat_flower,null,this);
        game.physics.arcade.overlap(this.player, this.flagpoles,this.reach_flagpole,null,this);

        game.physics.arcade.collide(this.bigplayer, this.continue_coinbricks,this.hitctbrick,null,this);
        game.physics.arcade.collide(this.bigplayer, this.starbricks,this.hitstarbrick,null,this);
        game.physics.arcade.collide(this.bigplayer, this.lifebricks,this.hitlifebrick,null,this);
        game.physics.arcade.collide(this.bigplayer, this.mushroom_and_flowerbricks,this.hitmfbrick,null,this);
        game.physics.arcade.collide(this.bigplayer, this.bricks,this.hitbrick,null,this);
        game.physics.arcade.collide(this.bigplayer, this.coin_bricks,this.hitcoinbrick,null,this);
        game.physics.arcade.collide(this.bigplayer, this.worldlayer);
        game.physics.arcade.overlap(this.bigplayer, this.pipe_entrys,this.at_pipe_entry,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.under_pipe_exits,this.at_under_pipe_exit,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.under_pipe_coins,this.eat_under_pipe_coin,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.green_mushroom,this.eat_green_mushroom,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.flower,this.eat_flower,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.flagpoles,this.reach_flagpole,null,this);

        game.physics.arcade.collide(this.fireplayer, this.continue_coinbricks,this.hitctbrick,null,this);
        game.physics.arcade.collide(this.fireplayer, this.starbricks,this.hitstarbrick,null,this);
        game.physics.arcade.collide(this.fireplayer, this.lifebricks,this.hitlifebrick,null,this);
        game.physics.arcade.collide(this.fireplayer, this.mushroom_and_flowerbricks,this.hitmfbrick,null,this);
        game.physics.arcade.collide(this.fireplayer, this.bricks,this.hitbrick,null,this);
        game.physics.arcade.collide(this.fireplayer, this.coin_bricks,this.hitcoinbrick,null,this);
        game.physics.arcade.collide(this.fireplayer, this.worldlayer);
        game.physics.arcade.overlap(this.fireplayer, this.pipe_entrys,this.at_pipe_entry,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.under_pipe_exits,this.at_under_pipe_exit,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.under_pipe_coins,this.eat_under_pipe_coin,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.green_mushroom,this.eat_green_mushroom,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.flower,this.eat_flower,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.flagpoles,this.reach_flagpole,null,this);

        game.physics.arcade.collide(this.goombas, this.bricks,this.goombahitbrick,null,this);
        game.physics.arcade.overlap(this.player, this.goombas,this.playerhitgoomba,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.goombas,this.playerhitgoomba,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.goombas,this.playerhitgoomba,null,this);
        game.physics.arcade.collide(this.goombas, this.worldlayer);
        game.physics.arcade.collide(this.goombas, this.mushroom_and_flowerbricks,this.goombahitbrick,null,this);
        
        game.physics.arcade.collide(this.koopas, this.worldlayer);
        game.physics.arcade.overlap(this.player, this.koopas,this.playerhitkoopa,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.koopas,this.playerhitkoopa,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.koopas,this.playerhitkoopa,null,this);

        game.physics.arcade.collide(this.green_mushroom, this.worldlayer);
        game.physics.arcade.collide(this.green_mushroom, this.lifebricks);

        game.physics.arcade.collide(this.koopas, this.goombas,this.koopahitgoomba,null,this);

        game.physics.arcade.collide(this.star, this.worldlayer);
        game.physics.arcade.overlap(this.player, this.star,this.eat_star,null,this);
        game.physics.arcade.overlap(this.bigplayer, this.star,this.eat_star,null,this);
        game.physics.arcade.overlap(this.fireplayer, this.star,this.eat_star,null,this);

        game.physics.arcade.collide(this.red_mushroom, this.worldlayer);
        game.physics.arcade.collide(this.red_mushroom, this.coinbricks);
        game.physics.arcade.collide(this.red_mushroom, this.mushroom_and_flowerbricks);
        game.physics.arcade.collide(this.red_mushroom, this.bricks);

        game.physics.arcade.collide(this.weapons , this.worldlayer);
        game.physics.arcade.overlap(this.weapons , this.goombas, this.weapon_hit_goomba,null,this);
        game.physics.arcade.overlap(this.weapons , this.koopas, this.weapon_hit_koopa,null,this);
        game.physics.arcade.collide(this.weapons , this.continue_coinbricks,this.kill_weapon,null,this);
        game.physics.arcade.collide(this.weapons, this.starbricks,this.kill_weapon,null,this);
        game.physics.arcade.collide(this.weapons, this.lifebricks,this.kill_weapon,null,this);
        game.physics.arcade.collide(this.weapons, this.mushroom_and_flowerbricks,this.kill_weapon,null,this);
        game.physics.arcade.collide(this.weapons, this.bricks,this.kill_weapon,null,this);
        game.physics.arcade.collide(this.weapons, this.coin_bricks,this.kill_weapon,null,this);
      
        


       
        
        if(!this.playerDie &&　!this.reachFlagpole)
            this.movePlayer();
        
      
        if(this.player.y >300 && this.who == "small"){
            
            if(!this.playerDie){
                this.playerDie = 1
                this.player_die_sound.play();
                this.level1_1_bgm.pause();
                console.log("small player out of world");
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        game.global.life -= 1;
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );
            }
            
        }
        else if(this.bigplayer.y >300 && this.who == "big"){
            
            if(!this.playerDie){
                this.playerDie = 1
                this.player_die_sound.play();
                this.level1_1_bgm.pause();
                console.log("big player out of world");
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        game.global.life -= 1;
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );
            }
            
        }
        else if(this.fireplayer.y > 300　&& this.who == "fire"){
            
            if(!this.playerDie){
                this.playerDie = 1
                this.player_die_sound.play();
                this.level1_1_bgm.pause();
                console.log("fire player out of world");
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        game.global.life -= 1;
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );
            }
            
        }

       
        if(!this.star_power_sound.isPlaying){
            this.star_power = 0;
            if(this.star_power_tween){
                this.star_power_tween.stop();
                this.star_power_tween2.stop();
            }
            this.player.tint = this.orig_tint;
            this.bigplayer.tint = this.orig_tint;
            this.fireplayer.tint = this.orig_tint;
        }
        
        
        //console.log("this.star_power = " + this.star_power);
        
        this.break_brick = 0;//為了big & fireplayer 撞到brick可以殺掉goomba

        
        
        

    },
    reach_flagpole: function(){
        if(this.reachFlagpole == 0){
            console.log("reach flagpole");
            this.reachFlagpole = 1;
            if(this.who == "small")
            {
                this.player.body.moves = false;
                this.player.body.enable = false;
                this.player.x+=2;
                this.player.loadTexture("small_mario_flag");
                this.player.animations.play("flag");
                this.level1_1_bgm.pause();
                this.flagpole_sound.play();
                game.add.tween(this.flag).to({x:this.flag.x,y:180},1200).start();
                tween = game.add.tween(this.player).to({x:this.player.x,y:175},800).start();
                tween.onComplete.add(function(){
                    this.player.animations.stop();
                    this.player.frame = 2;
                    game.camera.follow(null); //讓相機跟著player走
                    this.player.x+=12;
                    
                }, this);
                game.time.events.add(
                    1200, // Start callback after 1000ms
                    function(){
                        this.level_clear.play();
                        this.player.loadTexture("smallplayer");
                        this.player.x+=12;
                        this.player.y+=16;
                       
                        //this.player.y+=8;
                        game.camera.follow(this.player); //讓相機跟著player走
                        this.player.animations.play("rightwalk");
                        tween = game.add.tween(this.player).to({x:this.player.x+80,y:this.player.y},1000).start();
                        tween.onComplete.add(function(){
                            
                            this.player.kill();
                            game.camera.follow(null);
                        }, this);
                        
                    },
                    this
                );
                game.time.events.add(
                    7000, // Start callback after 1000ms
                    function(){
                        game.global.which_world = "1-2";
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );
            }
            else if(this.who == "big")
            {
                this.bigplayer.body.moves = false;
                this.bigplayer.body.enable = false;
                this.bigplayer.loadTexture("big_mario_flag");
                this.bigplayer.frame = 0;
                //.bigplayer.animations.play("flag");
                this.level1_1_bgm.pause();
                this.flagpole_sound.play();
                game.add.tween(this.flag).to({x:this.flag.x,y:180},1200).start();
                tween = game.add.tween(this.bigplayer).to({x:this.bigplayer.x,y:160},800).start();
                tween.onComplete.add(function(){
                    //this.bigplayer.animations.stop();
                    this.bigplayer.frame = 3;
                    this.bigplayer.x+=14;

                    game.camera.follow(null); //讓相機跟著player走
                    
                }, this);
                game.time.events.add(
                    1200, // Start callback after 1000ms
                    function(){
                        this.level_clear.play();
                        this.bigplayer.loadTexture("bigplayer");
                        this.bigplayer.x+=12;
                        this.bigplayer.y+=16;
                       
                        //this.player.y+=8;
                        game.camera.follow(this.bigplayer); //讓相機跟著player走
                        this.bigplayer.animations.play("rightwalk");
                        tween = game.add.tween(this.bigplayer).to({x:this.bigplayer.x+80,y:this.bigplayer.y},1000).start();
                        tween.onComplete.add(function(){
                            
                            this.bigplayer.kill();
                            game.camera.follow(null);
                        }, this);
                        
                    },
                    this
                );
                game.time.events.add(
                    7000, // Start callback after 1000ms
                    function(){
                        game.global.which_world = "1-2";
                        game.global.who = "big";
                        game.state.start('median');
                    },
                    this
                );
            }
            else if(this.who == "fire")
            {
                this.fireplayer.body.moves = false;
                this.fireplayer.body.enable = false;
                this.fireplayer.loadTexture("fire_mario_flag");
                this.fireplayer.frame = 0;
                //.bigplayer.animations.play("flag");
                this.level1_1_bgm.pause();
                this.flagpole_sound.play();
                game.add.tween(this.flag).to({x:this.flag.x,y:180},1200).start();
                tween = game.add.tween(this.fireplayer).to({x:this.fireplayer.x,y:160},800).start();
                tween.onComplete.add(function(){
                    //this.bigplayer.animations.stop();
                    this.fireplayer.frame = 3;
                    this.fireplayer.x+=14;

                    game.camera.follow(null); //讓相機跟著player走
                    
                }, this);
                game.time.events.add(
                    1200, // Start callback after 1000ms
                    function(){
                        this.level_clear.play();
                        this.fireplayer.loadTexture("fireplayer");
                        this.fireplayer.x+=12;
                        this.fireplayer.y+=16;
                       
                        //this.player.y+=8;
                        game.camera.follow(this.fireplayer); //讓相機跟著player走
                        this.fireplayer.animations.play("rightwalk");
                        tween = game.add.tween(this.fireplayer).to({x:this.fireplayer.x+80,y:this.fireplayer.y},1000).start();
                        tween.onComplete.add(function(){
                            
                            this.fireplayer.kill();
                            game.camera.follow(null);
                        }, this);
                        
                    },
                    this
                );
                game.time.events.add(
                    7000, // Start callback after 1000ms
                    function(){
                        game.global.which_world = "1-2";
                        game.global.who = "fire";
                        game.state.start('median');
                    },
                    this
                );
            }

        }
        
    },
    kill_weapon: function(weapon,brick){
        weapon.kill();
    },
    weapon_hit_goomba: function(weapon,goomba){
        if(goomba.scale.y != -1){
            weapon.kill();
            goomba.animations.stop(); 
            this.kick_sound.play();
            this.iskillinggoomba+=1;
            goomba.body.moves = false;
            goomba.anchor.y = 0.5;
        
            game.global.score += 100;
            console.log(game.global.score);
            if(game.global.score < 1000)
                this.score_text.setText("000"+game.global.score);
            else if(game.global.score < 10000 && game.global.score >= 1000 )
                this.score_text.setText("00"+game.global.score);
            else if(game.global.score < 100000 && game.global.score >= 10000 )
                this.score_text.setText("0"+game.global.score);
            else
                this.score_text.setText(game.global.score);



            text = game.add.text(goomba.x, goomba.y-30,"100", { font: '20px VT323', fill: '#ffffff' });
            tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
            tween_text.onComplete.add(function(){
                text.kill();
                
            }, this);
        
            goomba.scale.y = -1;
        
        
            game.add.tween(goomba).to({x:goomba.x,y:goomba.y-20},250).to({x:goomba.x,y:goomba.y+50},400).start().onComplete.add(function(){
                this.iskillinggoomba-=1;
                goomba.kill();
                
            }, this);;
        }

    },
    weapon_hit_koopa: function(weapon,koopa){
        if(koopa.scale.y != -1){
            weapon.kill();
            koopa.animations.stop(); 
            this.kick_sound.play();
            koopa.body.moves = false;
            koopa.frame = 10;
            koopa.anchor.y = 0.5;
            //weapon.animations.play("hit");
            game.global.score += 200;
            //console.log(game.global.score);
            if(game.global.score < 1000)
                this.score_text.setText("000"+game.global.score);
            else if(game.global.score < 10000 && game.global.score >= 1000 )
                this.score_text.setText("00"+game.global.score);
            else if(game.global.score < 100000 && game.global.score >= 10000 )
                this.score_text.setText("0"+game.global.score);
            else
                this.score_text.setText(game.global.score);

            text = game.add.text(koopa.x, koopa.y-30,"200", { font: '20px VT323', fill: '#ffffff' });
            tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
            tween_text.onComplete.add(function(){
                text.kill();
                
            }, this);
        
            koopa.scale.y = -1;
        
        
            game.add.tween(koopa).to({x:koopa.x,y:koopa.y+50},500).start().onComplete.add(function(){
                koopa.kill();
               
            }, this);;
        } 

    },
    eat_star: function(){
        this.star.animations.play('anim');
        console.log("eat star");
        this.star.kill();
        game.global.score += 1000;
        if(game.global.score < 1000)
            this.score_text.setText("000"+game.global.score);
        else if(game.global.score < 10000 && game.global.score >= 1000 )
            this.score_text.setText("00"+game.global.score);
        else if(game.global.score < 100000 && game.global.score >= 10000 )
            this.score_text.setText("0"+game.global.score);
        else
            this.score_text.setText(game.global.score);

        //console.log("score = " + game.global.score );
        this.Powerup_sound.play();
        if(this.who == "small"){
        this.player.tint = this.star_tint;
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.star_power_sound.play();
                this.level1_1_bgm.pause();
                this.star_power = 1;
                this.player.tint = this.star_tint;
                this.star_power_tween = game.add.tween(this.player).to({tint:this.star_tint},100).to({tint:this.orig_tint},100).loop().start();
                this.star_power_tween2 = game.add.tween(this.bigplayer).to({tint:this.star_tint},100).to({tint:this.orig_tint},100).loop().start();
            },
            this
        );
        game.time.events.add(
            13000, // Start callback after 1000ms
            function(){
                this.level1_1_bgm.resume();
            },
            this
        );

       

                text = game.add.text(this.player.x, this.player.y-30,"1000", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
        }
        else if(this.who == "big"){
            this.bigplayer.tint = this.star_tint;
            game.time.events.add(
                1000, // Start callback after 1000ms
                function(){
                    this.star_power_sound.play();
                    this.level1_1_bgm.pause();
                    this.star_power = 1;
                    this.bigplayer.tint = this.star_tint;
                    this.star_power_tween = game.add.tween(this.bigplayer).to({tint:this.star_tint},100).to({tint:this.orig_tint},100).loop().start();
                    this.star_power_tween2 = game.add.tween(this.fireplayer).to({tint:this.star_tint},100).to({tint:this.orig_tint},100).loop().start();
                },
                this
            );
            game.time.events.add(
                13000, // Start callback after 1000ms
                function(){
                    this.level1_1_bgm.resume();
                },
                this
            );
    
           
    
                    text = game.add.text(this.bigplayer.x, this.bigplayer.y-30,"1000", { font: '20px VT323', fill: '#ffffff' });
                    tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                    tween_text.onComplete.add(function(){
                        text.kill();
                        
                    }, this);
        }
        else if(this.who == "fire"){
            this.fireplayer.tint = this.star_tint;
            game.time.events.add(
                1000, // Start callback after 1000ms
                function(){
                    this.star_power_sound.play();
                    this.level1_1_bgm.pause();
                    this.star_power = 1;
                    this.fireplayer.tint = this.star_tint;
                    this.star_power_tween = game.add.tween(this.fireplayer).to({tint:this.star_tint},100).to({tint:this.orig_tint},100).loop().start();
                    this.star_power_tween2 = game.add.tween(this.bigplayer).to({tint:this.star_tint},100).to({tint:this.orig_tint},100).loop().start();
                },
                this
            );
            game.time.events.add(
                13000, // Start callback after 1000ms
                function(){
                    this.level1_1_bgm.resume();
                },
                this
            );
    
           
    
                    text = game.add.text(this.fireplayer.x, this.fireplayer.y-30,"1000", { font: '20px VT323', fill: '#ffffff' });
                    tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                    tween_text.onComplete.add(function(){
                        text.kill();
                        
                    }, this);
        }
        


    },
    koopahitgoomba: function(koopa,goomba){
        if(goomba.inCamera && goomba.scale.y == 1)
        {

                this.iskillinggoomba += 1;
                this.kick_sound.play();
                goomba.scale.y = -1;
                game.global.score += 500;
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);

                //console.log(game.global.score);
                tween_goomba = game.add.tween(goomba).to({x:goomba.x,y:goomba.y-20},200).to({x:goomba.x,y:240},500).start();
                text = game.add.text(goomba.x, goomba.y-50,"500", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill() ;
                }, this);
                tween_goomba.onComplete.add(function(){
                    goomba.kill() ;
                    this.iskillinggoomba -= 1;
                        
                }, this);
                
            
        }

    },
    eat_red_mushroom: function(){
       
        this.Powerup_sound.play();
        this.red_mushroom.body.enable = false;
        this.red_mushroom.alpha = 0;
        this.game.global.score += 1000;

        if(game.global.score < 1000)
            this.score_text.setText("000"+game.global.score);
        else if(game.global.score < 10000 && game.global.score >= 1000 )
            this.score_text.setText("00"+game.global.score);
        else if(game.global.score < 100000 && game.global.score >= 10000 )
            this.score_text.setText("0"+game.global.score);
        else
            this.score_text.setText(game.global.score);

        
        this.who = 'big';

        console.log("this.who == "+ this.who);

        this.bigplayer.x = this.player.x;
        this.bigplayer.y = this.player.y-16;
        this.bigplayer.body.enable = true;
        this.bigplayer.alpha = 1;
        this.bigplayer.body.gravity.y = 300;

        game.camera.follow(this.bigplayer); //讓相機跟著player走
    
        text = game.add.text(this.bigplayer.x, this.bigplayer.y-30,"1000", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
    },
    eat_flower: function(player,flower){
       
        this.Powerup_sound.play();
        this.flower.alpha = 0;
        this.flower.body.enable = false;
        this.game.global.score += 1000;

        if(game.global.score < 1000)
            this.score_text.setText("000"+game.global.score);
        else if(game.global.score < 10000 && game.global.score >= 1000 )
            this.score_text.setText("00"+game.global.score);
        else if(game.global.score < 100000 && game.global.score >= 10000 )
            this.score_text.setText("0"+game.global.score);
        else
            this.score_text.setText(game.global.score);
       
        this.who = 'fire';

        console.log("this.who == "+ this.who);

        this.fireplayer.x = player.x;
        this.fireplayer.y = player.y-16;
        this.fireplayer.body.enable = true;
        this.fireplayer.alpha = 1;
        this.fireplayer.body.gravity.y = 300;

        game.camera.follow(this.fireplayer); //讓相機跟著player走
    
        text = game.add.text(this.fireplayer.x, this.fireplayer.y-30,"1000", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
    },
    eat_green_mushroom: function(){
        //console.log("add life");
    if(this.who == "small"){
        this.add_life_sound.play();
        this.green_mushroom.kill();
        game.global.life += 1;
        console.log("life = " + game.global.life );

        text = game.add.text(this.player.x, this.player.y-30,"1 up", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
    }
    else if(this.who == "big"){
        this.add_life_sound.play();
        this.green_mushroom.kill();
        game.global.life += 1;
        console.log("life = " + game.global.life );

        text = game.add.text(this.bigplayer.x, this.bigplayer.y-45,"1 up", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
    }
    else if(this.who == "fire"){
        this.add_life_sound.play();
        this.green_mushroom.kill();
        game.global.life += 1;
        console.log("life = " + game.global.life );

        text = game.add.text(this.fireplayer.x, this.fireplayer.y-45,"1 up", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-40},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
                
    }

    },
    playerhitkoopa: function(player,koopa){
        if(this.alpha_state == 0){
        if(this.star_power)
        {
            if(koopa.scale.y != -1){
                koopa.animations.stop(); 
                this.kick_sound.play();
                koopa.body.moves = false;
                koopa.frame = 10;
                koopa.anchor.y = 0.5;
            
                game.global.score += 200;
                //console.log(game.global.score);
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);

                text = game.add.text(koopa.x, koopa.y-30,"200", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
            
                koopa.scale.y = -1;
            
            
                game.add.tween(koopa).to({x:koopa.x,y:koopa.y+50},500).start().onComplete.add(function(){
                    koopa.kill();
                }, this);;
        }
            
        }
        else if(this.player.body.touching.down ||this.bigplayer.body.touching.down || this.fireplayer.body.touching.down )
        {
            if(!this.koopa_squished){
                this.koopa_squished = 1;
                koopa.body.moves = false;
                this.player.moves = false;
                this.goomba_die.play();
                game.global.score += 100;
                //console.log(game.global.score);
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);

                console.log("player踩到koopa");
                koopa.animations.stop(); 
                
                koopa.frame = 10;
                
                player_tween = game.add.tween(player).to({x:player.x+20,y:player.y},200).start();
                //player_tween = game.add.tween(player).to({x:player.x+15,y:player.y-16},500).start();
                player_tween.onComplete.add(function(){
                    this.player.moves = true;
                    
                }, this);
                text = game.add.text(koopa.x, koopa.y-30,"100", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
                
            }
        }
        else if(koopa.body.touching.left)
        {
            if(this.koopa_squished &&　!(this.koopa_kicked)){
                this.kick_sound.play();
                game.global.score += 500;
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);
                //console.log(game.global.score);
                this.koopa_kicked = 1;
                koopa.body.moves = true;
                koopa.body.velocity.x = 150;
                koopa.animations.play('squish');
                console.log("to right");
                text = game.add.text(koopa.x, koopa.y-30,"500", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);

            }
            else if(!this.playerDie && !(this.koopa_squished) && this.who == 'small'){
                //console.log("stop");
                
                player.body.moves = false;
                koopa.body.moves = false;
                koopa.animations.stop();
                this.playerDie = 1;
                console.log("撞到koopa");
                this.player_die_sound.play();
                this.player.animations.stop();
                this.level1_1_bgm.pause();
                this.player.frame = 6;
                player.body.enable = false;
                game.add.tween(this.player).to({x:player.x,y:player.y-30},300).to({x:player.x,y:250},800).start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        game.global.life -= 1;
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );

            }
            else if(!this.playerDie && !(this.koopa_squished) && this.who == 'big'){
                //console.log("stop");
                this.go_into_pipe_sound.play();
                this.alpha_state = 1;
                this.who = "small";
                this.player.x = this.bigplayer.x;
                this.player.y = this.bigplayer.y;
                this.player.body.enable = true;
                this.bigplayer.animations.stop();
                this.bigplayer.body.enable = false;
                this.bigplayer.alpha = 0;
                console.log("大人撞到koopa");
                game.camera.follow(this.player); //讓相機跟著player走
                this.tweenalpha = game.add.tween(this.player).to({alpha: 0},12).to({alpha: 1},12).loop().start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        this.tweenalpha.stop();
                        this.player.alpha = 1;
                        //console.log("alpha finish");
                        //console.log("this.who = " +　this.who)
                        
                        this.alpha_state = 0;
                        //console.log("this.alpha_state = "+ this.alpha_state)
                    },
                    this
                );

            }
            else if(!this.playerDie && !(this.koopa_squished) && this.who == 'fire'){
                //console.log("stop");
                this.go_into_pipe_sound.play();
                this.alpha_state = 1;
                game.camera.follow(this.bigplayer); //讓相機跟著player走
                this.who = "big";
                this.bigplayer.x = this.fireplayer.x;
                this.bigplayer.y = this.fireplayer.y;
                this.bigplayer.body.enable = true;
                this.bigplayer.animations.stop();
                this.fireplayer.animations.stop();
                this.fireplayer.body.enable = false;
                this.fireplayer.alpha = 0;
                console.log("射擊人撞到koopa");
                
                this.tweenalpha = game.add.tween(this.bigplayer).to({alpha: 0},12).to({alpha: 1},12).loop().start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        this.tweenalpha.stop();
                        this.bigplayer.alpha = 1;
                        //console.log("alpha finish");
                        //console.log("this.who = " +　this.who)
                        
                        this.alpha_state = 0;
                        //console.log("this.alpha_state = "+ this.alpha_state)
                    },
                    this
                );

            }
        }
        else if(koopa.body.touching.right)
        {
            if(this.koopa_squished &&　!(this.koopa_kicked)){
                this.kick_sound.play();
                game.global.score += 500;
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);
                //console.log(game.global.score);
                this.koopa_kicked = 1;
                koopa.body.moves = true;
                koopa.body.velocity.x = -150;
                koopa.animations.play('squish');
                console.log("to left");
                text = game.add.text(koopa.x, koopa.y-30,"500", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
            }
            else if(!this.playerDie && !(this.koopa_squished) && this.who == 'small'){
                //console.log("stop");
                player.body.moves = false;
                koopa.body.moves = false;
                koopa.animations.stop();
                this.playerDie = 1;
                console.log("撞到koopa");
                this.player_die_sound.play();
                this.level1_1_bgm.pause();
                this.player.animations.stop();
                this.player.frame = 6;
                player.body.enable = false;
                game.add.tween(this.player).to({x:player.x,y:player.y-30},300).to({x:player.x,y:250},800).start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        game.global.life -= 1;
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );

            }
            else if(!this.playerDie && !(this.koopa_squished) && this.who == 'big'){
                //console.log("stop");
                this.go_into_pipe_sound.play();
                this.alpha_state = 1;
                this.who = "small";
                this.player.x = this.bigplayer.x;
                this.player.y = this.bigplayer.y;
                this.player.body.enable = true;
                this.bigplayer.animations.stop();
                this.bigplayer.body.enable = false;
                this.bigplayer.alpha = 0;
                game.camera.follow(this.player); //讓相機跟著player走
                console.log("大人撞到koopa");
                
                this.tweenalpha = game.add.tween(this.player).to({alpha: 0},12).to({alpha: 1},12).loop().start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        this.tweenalpha.stop();
                        this.player.alpha = 1;
                        //console.log("alpha finish");
                        //console.log("this.who = " +　this.who)
                        
                        this.alpha_state = 0;
                        //console.log("this.alpha_state = "+ this.alpha_state)
                    },
                    this
                );

            }
            else if(!this.playerDie && !(this.koopa_squished) && this.who == 'fire'){
                //console.log("stop");
                this.go_into_pipe_sound.play();
                this.alpha_state = 1;
                this.who = "big";
                this.bigplayer.x = this.fireplayer.x;
                this.bigplayer.y = this.fireplayer.y;
                this.bigplayer.body.enable = true;
                this.bigplayer.animations.stop();
                this.fireplayer.animations.stop();
                this.fireplayer.body.enable = false;
                this.fireplayer.alpha = 0;
                console.log("射擊人撞到koopa");
                game.camera.follow(this.bigplayer); //讓相機跟著player走
                
                this.tweenalpha = game.add.tween(this.bigplayer).to({alpha: 0},12).to({alpha: 1},12).loop().start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        this.tweenalpha.stop();
                        this.bigplayer.alpha = 1;
                        //console.log("alpha finish");
                        //console.log("this.who = " +　this.who)
                        
                        this.alpha_state = 0;
                        //console.log("this.alpha_state = "+ this.alpha_state)
                    },
                    this
                );

            }
        }
    }
    },
    eat_under_pipe_coin: function(player,coin){
        this.eat_coin_sound.play();
        coin.kill();
        game.global.coin += 1;
        game.global.score += 200;
        if(game.global.score < 1000)
            this.score_text.setText("000"+game.global.score);
        else if(game.global.score < 10000 && game.global.score >= 1000 )
            this.score_text.setText("00"+game.global.score);
        else if(game.global.score < 100000 && game.global.score >= 10000 )
            this.score_text.setText("0"+game.global.score);
        else
            this.score_text.setText(game.global.score);


        if(game.global.coin < 10)
            this.money_text.setText("x"+"0"+game.global.coin);
        else 
            this.money_text.setText("x"+game.global.coin);

        //console.log( "game.global.coin = " +  game.global.coin);
        //console.log( "game.global.score = " +  game.global.score);
    },
    at_pipe_entry: function(){

        console.log("at_pipe_entry");
    if(this.who == "small"){
        if (this.cursors.down.isDown){

            this.player.body.enable = false;
            this.level1_1_bgm.pause();
            this.go_into_pipe_sound.play();
            
            
            tween_under_pipe = game.add.tween(this.player).to({x:this.player.x,y:this.player.y+20},1000).start();
            tween_under_pipe.onComplete.add(function(){
                this.player.x = this.under_pipe_entry_pos_x;
                this.player.y = this.under_pipe_entry_pos_y;
                this.player.body.enable = true;
                this.under_pipe_bgm.play();
                
                game.camera.follow(null); //讓相機跟著player走
                game.camera.x = 3632;
              
               
                //this.black_bg.alpha = 0;
                
            }, this);

            

        }
    }
    else if(this.who == "big"){
        if (this.cursors.down.isDown){

            this.bigplayer.body.enable = false;
            this.go_into_pipe_sound.play();
            this.level1_1_bgm.pause();
            tween_under_pipe = game.add.tween(this.bigplayer).to({x:this.bigplayer.x,y:this.bigplayer.y+20},1000).start();
            tween_under_pipe.onComplete.add(function(){
                this.bigplayer.x = this.under_pipe_entry_pos_x;
                this.bigplayer.y = this.under_pipe_entry_pos_y;
                this.bigplayer.body.enable = true;
                this.under_pipe_bgm.play();
                
                game.camera.follow(null); //讓相機跟著player走
                game.camera.x = 3632;
               
                //this.black_bg.alpha = 0;
                
            }, this);

            

        }
    }
    else if(this.who == "fire"){
        if (this.cursors.down.isDown){

            this.fireplayer.body.enable = false;
            this.go_into_pipe_sound.play();
            this.level1_1_bgm.pause();
            tween_under_pipe = game.add.tween(this.fireplayer).to({x:this.fireplayer.x,y:this.fireplayer.y+20},1000).start();
            tween_under_pipe.onComplete.add(function(){
                this.fireplayer.x = this.under_pipe_entry_pos_x;
                this.fireplayer.y = this.under_pipe_entry_pos_y;
                this.fireplayer.body.enable = true;
                this.under_pipe_bgm.play();
                
                game.camera.follow(null); //讓相機跟著player走
                game.camera.x = 3632;
               
                //this.black_bg.alpha = 0;
                
            }, this);

            

        }
    }
    },
    at_under_pipe_exit: function(){
    if(this.who == "small"){
        this.under_pipe_exit_counter += 1;
        if(this.under_pipe_exit_counter > 20 &&　this.cursors.right.isDown)
        {
            console.log("go");
            this.player.animations.play('righttwalk');
            this.player.body.enable = false;
            this.go_into_pipe_sound.play();
            tween_into_pipe = game.add.tween(this.player).to({x:this.player.x + 20　,y:this.player.y},1000).start();
            tween_into_pipe.onComplete.add(function(){
                this.under_pipe_bgm.pause();
                
                this.player.x = this.pipe_exit_pos_x;
                this.player.y = this.pipe_exit_pos_y;
                game.camera.follow(this.player);
                tween_out_pipe = game.add.tween(this.player).to({x:this.player.x,y:this.player.y - 18},1000).start();
                tween_out_pipe.onComplete.add(function(){
                    this.player.body.enable = true; 
                    this.level1_1_bgm.resume(); 
                }, this);

                game.camera.follow(this.player); //讓相機跟著player走
               
                //this.black_bg.alpha = 0;
                
            }, this);
           
        }
    }
    else if(this.who == "big"){
        this.under_pipe_exit_counter += 1;
        if(this.under_pipe_exit_counter > 20 &&　this.cursors.right.isDown)
        {
            console.log("go");
            this.bigplayer.animations.play('righttwalk');
            this.bigplayer.body.enable = false;
            this.go_into_pipe_sound.play();
            tween_into_pipe = game.add.tween(this.bigplayer).to({x:this.bigplayer.x + 20　,y:this.bigplayer.y},1000).start();
            tween_into_pipe.onComplete.add(function(){
                this.under_pipe_bgm.pause();
                
                this.bigplayer.x = this.pipe_exit_pos_x;
                this.bigplayer.y = this.pipe_exit_pos_y;
                game.camera.follow(this.bigplayer);
                tween_out_pipe = game.add.tween(this.bigplayer).to({x:this.bigplayer.x,y:this.bigplayer.y - 18},1000).start();
                tween_out_pipe.onComplete.add(function(){
                    this.bigplayer.body.enable = true;  
                    this.level1_1_bgm.resume();
                }, this);

                game.camera.follow(this.bigplayer); //讓相機跟著player走
               
                //this.black_bg.alpha = 0;
                
            }, this);
           
        }
    }
    else if(this.who == "fire"){
        this.under_pipe_exit_counter += 1;
        if(this.under_pipe_exit_counter > 20 &&　this.cursors.right.isDown)
        {
            console.log("go");
            this.fireplayer.animations.play('righttwalk');
            this.fireplayer.body.enable = false;
            this.go_into_pipe_sound.play();
            tween_into_pipe = game.add.tween(this.fireplayer).to({x:this.fireplayer.x + 20　,y:this.fireplayer.y},1000).start();
            tween_into_pipe.onComplete.add(function(){
                this.under_pipe_bgm.pause();
                
                this.fireplayer.x = this.pipe_exit_pos_x;
                this.fireplayer.y = this.pipe_exit_pos_y;
                game.camera.follow(this.fireplayer);
                tween_out_pipe = game.add.tween(this.fireplayer).to({x:this.fireplayer.x,y:this.fireplayer.y - 18},1000).start();
                tween_out_pipe.onComplete.add(function(){
                    this.fireplayer.body.enable = true; 
                    this.level1_1_bgm.resume(); 
                }, this);

                game.camera.follow(this.fireplayer); //讓相機跟著player走
               
                //this.black_bg.alpha = 0;
                
            }, this);
           
        }
    }
    },
    goombahitbrick: function(goomba,brick){
        //console.log("hit");
            //goomba.body.velocity.x = 0;
            goomba.body.gravity.y = 0;
            goomba.body.velocity.y = 0;
            
            if (!this.player.body.onFloor() && this.player.body.touching.up && (this.player.x - goomba.x <= 10 && this.player.x - goomba.x >= -10) || !this.bigplayer.body.onFloor() && this.bigplayer.body.touching.up && (this.bigplayer.x - goomba.x <= 10 && this.bigplayer.x - goomba.x >= -10) || !this.fireplayer.body.onFloor() && this.fireplayer.body.touching.up && (this.fireplayer.x - goomba.x <= 10 && this.fireplayer.x - goomba.x >= -10))
            {
                //console.log(this.player.x - goomba.x);
                //console.log("playerhitbrick");
                console.log("brick hit goomba!")
                this.iskillinggoomba = 1;
                this.kick_sound.play();
                goomba.scale.y = -1;
                game.global.score += 100;
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);
                //console.log(game.global.score);
                tween_goomba = game.add.tween(goomba).to({x:goomba.x,y:goomba.y-10},200).to({x:goomba.x,y:240},1000).start();
                text = game.add.text(goomba.x, goomba.y-50,"100", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill() ;
                }, this);
                tween_goomba.onComplete.add(function(){
                    goomba.kill() ;
                    this.iskillinggoomba = 0;
                        
                }, this);
                if(brick.key == brick)
                    brick.kill();
                
            }
           
        
            
    },
    playerhitgoomba: function(player,goomba){
        if(this.alpha_state == 0){
        if(this.star_power)
        {
            if(goomba.scale.y != -1){
                goomba.animations.stop(); 
                this.kick_sound.play();
                goomba.body.moves = false;
                //goomba.frame = 10;
                goomba.anchor.y = 0.5;
            
                game.global.score += 100;
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);
                //console.log(game.global.score);
                text = game.add.text(goomba.x, goomba.y-30,"100", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                    
                }, this);
            
                goomba.scale.y = -1;
            
            
                game.add.tween(goomba).to({x:goomba.x,y:goomba.y-30},250).to({x:goomba.x,y:goomba.y+50},250).start().onComplete.add(function(){
                    goomba.kill();
                }, this);;
        }
            
        }
        else if(player.body.touching.down &&　!(this.playerOnbrick) )
        {
            
            if(goomba.frame != 2){
                goomba.body.moves = false;
                this.iskillinggoomba += 1;
                this.goomba_die.play();
                game.global.score += 100;
                if(game.global.score < 1000)
                    this.score_text.setText("000"+game.global.score);
                else if(game.global.score < 10000 && game.global.score >= 1000 )
                    this.score_text.setText("00"+game.global.score);
                else if(game.global.score < 100000 && game.global.score >= 10000 )
                    this.score_text.setText("0"+game.global.score);
                else
                    this.score_text.setText(game.global.score);
                //console.log(game.global.score);
                console.log("player踩到goomba");
                goomba.animations.stop(); 
                goomba.frame = 2;
               
                text = game.add.text(goomba.x, goomba.y-50,"100", { font: '20px VT323', fill: '#ffffff' });
                tween_text = game.add.tween(text).to({x:text.x,y:text.y-20},600).to({alpha: 0},10).start();
                tween_text.onComplete.add(function(){
                    text.kill();
                }, this);
                game.add.tween(player).to({x:player.x,y:player.y-10},200).to({x:player.x,y:player.y},200).start();
                game.time.events.add(
                    1000, // Start callback after 1000ms
                    function(){goomba.kill(); this.iskillinggoomba -= 1; }, // Callback 變成固定的磚塊
                    this
                );
                
            }
        }
        else if(!this.iskillinggoomba &&　!(this.playerDie) && this.who == "small")
        {
            
            //if(!this.iskillinggoomba &&　!(this.playerDie)){
                //console.log("stop");
                player.body.moves = false;
                goomba.body.moves = false;
                this.playerDie = 1;
                this.iskillinggoomba = 1;
                console.log("撞到goomba");
                this.player_die_sound.play();
                this.level1_1_bgm.pause();
                this.player.animations.stop();
                this.player.frame = 6;
                player.body.enable = false;
                game.add.tween(this.player).to({x:player.x,y:player.y-30},300).to({x:player.x,y:250},800).start();
                game.time.events.add(
                    2000, // Start callback after 1000ms
                    function(){
                        game.global.life -= 1;
                        game.global.who = "small";
                        game.state.start('median');
                    },
                    this
                );

            //}
        }
        else if(!this.iskillinggoomba &&　!(this.playerDie) && this.who == "big")
        {
            
            this.go_into_pipe_sound.play();
            this.alpha_state = 1;
            this.who = "small";
            this.player.x = this.bigplayer.x;
            this.player.y = this.bigplayer.y;
            this.player.body.enable = true;
            this.bigplayer.animations.stop();
            this.bigplayer.body.enable = false;
            this.bigplayer.alpha = 0;
            console.log("大人撞到goomba");
            game.camera.follow(this.player); //讓相機跟著player走
            this.tweenalpha = game.add.tween(this.player).to({alpha: 0},12).to({alpha: 1},12).loop().start();
            game.time.events.add(
                2000, // Start callback after 1000ms
                function(){
                    this.tweenalpha.stop();
                    this.player.alpha = 1;
                    //console.log("alpha finish");
                    //console.log("this.who = " +　this.who)
                    
                    this.alpha_state = 0;
                    //console.log("this.alpha_state = "+ this.alpha_state)
                },
                this
            );
        }
        else if(!this.iskillinggoomba &&　!(this.playerDie) && this.who == "fire")
        {
            
            this.go_into_pipe_sound.play();
            this.alpha_state = 1;
            this.who = "big";
            this.bigplayer.x = this.fireplayer.x;
            this.bigplayer.y = this.fireplayer.y;
            this.bigplayer.body.enable = true;
            this.fireplayer.animations.stop();
            this.fireplayer.body.enable = false;
            this.fireplayer.alpha = 0;
            console.log("射擊人撞到goomba");
            game.camera.follow(this.bigplayer); //讓相機跟著player走
            this.tweenalpha = game.add.tween(this.bigplayer).to({alpha: 0},12).to({alpha: 1},12).loop().start();
            game.time.events.add(
                2000, // Start callback after 1000ms
                function(){
                    this.tweenalpha.stop();
                    this.bigplayer.alpha = 1;
                    //console.log("alpha finish");
                    //console.log("this.who = " +　this.who)
                    
                    this.alpha_state = 0;
                    //console.log("this.alpha_state = "+ this.alpha_state)
                },
                this
            );
        }
        }
        
    },
    movePlayer: function(){
        //console.log(this.player.body.velocity.x);
    if(this.who == "small"){
        if (this.cursors.up.isDown)
        {
            if (this.player.body.onFloor() || this.player.body.touching.down)
            {
                this.jumpSmall_sound.play();
                this.under_pipe_exit_counter = 0;
                this.playeridlecounter = 0;
                if(this.playerdirection == "right")
                    this.player.animations.play('rightjump');
                else if(this.playerdirection == "left")
                    this.player.animations.play('leftjump');
                this.player.body.velocity.y = -205;
            }
        }
        else if (this.cursors.left.isDown)
        {
            this.under_pipe_exit_counter = 0;
            
            //this.player.scale.x = 1;
            this.playerdirection = "left";
            if (!this.player.body.onFloor() && !this.player.body.touching.down )
            {
                this.player.animations.stop(); 
                if(this.playerdirection== "rightjump")
                    this.player.frame = 1;
                else if(this.playerdirection == "leftjump")
                    this.player.frame = 11;
            }
            else if(this.player.body.velocity.x > -10 && this.playeridlecounter == 0 && this.playerdirection == "left" && this.player.body.velocity.x != 0) 
                this.player.frame = 12;
            else{
                this.player.animations.play('leftwalk');
                this.playeridlecounter = 0;
            }
           
            if(this.player.body.velocity.x >(-110))
                this.player.body.velocity.x -= 10;
            else
                this.player.body.velocity.x = -110;

        }
        else if (this.cursors.right.isDown)
        {
            
            //this.player.scale.x = 1;
            this.playerdirection = "right";
            if (!this.player.body.onFloor() && !this.player.body.touching.down )
            {
                this.player.animations.stop(); 
                if(this.playerdirection== "rightjump")
                    this.player.frame = 1;
                else if(this.playerdirection == "leftjump")
                    this.player.frame = 11;
            }
            else if(this.player.body.velocity.x < 10  && this.playeridlecounter == 0 && this.playerdirection == "right" && this.player.body.velocity.x != 0) 
                this.player.frame = 0;
            else{
                this.player.animations.play('rightwalk');
                this.playeridlecounter = 0;
            }
           
            if(this.player.body.velocity.x < 110)
                this.player.body.velocity.x += 10;
            else
                this.player.body.velocity.x = 110;

        }
        else
        {
            this.playeridlecounter += 1;
           // if(this.player.body.velocity.x < 1 && this.player.body.velocity.x > 0 || this.player.body.velocity.x > -1 && this.player.body.velocity.x < 0 )
                //this.player.body.velocity.x = 0;
            if(this.player.body.velocity.x > 0)
                this.player.body.velocity.x -= this.player.body.velocity.x/2;
            else if(this.player.body.velocity.x < 0)
                this.player.body.velocity.x += this.player.body.velocity.x/2;
            
            this.player.body.velocity.x = 0;
            
            //his.player.body.velocity.x = 0;
            //this.player.loadTexture 
            this.player.animations.stop(); 
            if(this.playerdirection== "right")
                this.player.frame = 1;
            else if(this.playerdirection == "left")
                this.player.frame = 11;
        
        }
    }
    else if(this.who == "big"){
        if (this.cursors.up.isDown)
        {
            if (this.bigplayer.body.onFloor() || this.bigplayer.body.touching.down)
            {
                this.jumpBig_sound.play();
                this.under_pipe_exit_counter = 0;
                this.playeridlecounter = 0;
                if(this.playerdirection == "right")
                    this.bigplayer.animations.play('rightjump');
                else if(this.playerdirection == "left")
                    this.bigplayer.animations.play('leftjump');
                this.bigplayer.body.velocity.y = -205;
            }
        }
        else if (this.cursors.down.isDown)
        {
            if (this.bigplayer.body.onFloor() || this.bigplayer.body.touching.down ){
                this.bigplayer.frame = 6;
                this.bigplayer.body.velocity.x = 0;
            }
        }
        else if (this.cursors.left.isDown)
        {
            this.under_pipe_exit_counter = 0;
            
            //this.player.scale.x = 1;
            this.playerdirection = "left";
            if (!this.bigplayer.body.onFloor() && !this.bigplayer.body.touching.down )
            {
                this.bigplayer.animations.stop(); 
                if(this.playerdirection== "rightjump")
                    this.bigplayer.frame = 1;
                else if(this.playerdirection == "leftjump")
                    this.bigplayer.frame = 11;
            }
            else if(this.bigplayer.body.velocity.x > -10 && this.playeridlecounter == 0 && this.playerdirection == "left" && this.bigplayer.body.velocity.x != 0) 
                this.bigplayer.frame = 12;
            else{
                this.bigplayer.animations.play('leftwalk');
                this.playeridlecounter = 0;
            }
           
            if(this.bigplayer.body.velocity.x >(-110))
                this.bigplayer.body.velocity.x -= 10;
            else
                this.bigplayer.body.velocity.x = -110;

        }
        else if (this.cursors.right.isDown)
        {
            
            //this.player.scale.x = 1;
            this.playerdirection = "right";
            if (!this.bigplayer.body.onFloor() && !this.bigplayer.body.touching.down )
            {
                
                this.bigplayer.animations.stop(); 
                if(this.playerdirection== "rightjump")
                    this.bigplayer.frame = 1;
                else if(this.playerdirection == "leftjump")
                    this.bigplayer.frame = 11;
            }
            else if(this.bigplayer.body.velocity.x < 10  && this.playeridlecounter == 0 && this.playerdirection == "right" && this.bigplayer.body.velocity.x != 0){
            
                this.bigplayer.frame = 0;
            }
            else{
                
                this.bigplayer.animations.play('rightwalk');
                this.playeridlecounter = 0;
            }
           
            if(this.bigplayer.body.velocity.x < 110)
                this.bigplayer.body.velocity.x += 10;
            else
                this.bigplayer.body.velocity.x = 110;

        }
        else
        {
            this.playeridlecounter += 1;
           // if(this.player.body.velocity.x < 1 && this.player.body.velocity.x > 0 || this.player.body.velocity.x > -1 && this.player.body.velocity.x < 0 )
                //this.player.body.velocity.x = 0;
            if(this.bigplayer.body.velocity.x > 0)
                this.bigplayer.body.velocity.x -= this.bigplayer.body.velocity.x/2;
            else if(this.bigplayer.body.velocity.x < 0)
                this.bigplayer.body.velocity.x += this.bigplayer.body.velocity.x/2;
            
            this.bigplayer.body.velocity.x = 0;
            
            //his.player.body.velocity.x = 0;
            //this.player.loadTexture 
            this.bigplayer.animations.stop(); 
            if(this.playerdirection== "right")
                this.bigplayer.frame = 1;
            else if(this.playerdirection == "left")
                this.bigplayer.frame = 11;
        
        }
    }
    else if(this.who == "fire"){
        
        if(this.space_key.isDown && this.space_down == 0){
            console.log("space");
            this.fireball = this.weapons.getFirstDead(); 
            this.shoot_sound.play();
            
            //if(this.fireball){
            this.space_down = 1;
            
            this.fireball.reset(this.fireplayer.x, this.fireplayer.y);
            
            this.fireball.body.gravity.y = 500;
            this.fireball.body.bounce.y = 1;
            this.fireball.checkWorldBounds = true; 
            this.fireball.outOfBoundsKill = true;
            if(this.playerdirection == "right")
                this.fireball.body.velocity.x = 150;
            else
                this.fireball.body.velocity.x = -150;

                
        }
        else if (this.cursors.up.isDown)
        {
            if (this.fireplayer.body.onFloor() || this.fireplayer.body.touching.down)
            {
                this.jumpBig_sound.play();
                this.under_pipe_exit_counter = 0;
                this.playeridlecounter = 0;
                if(this.playerdirection == "right")
                    this.fireplayer.animations.play('rightjump');
                else if(this.playerdirection == "left")
                    this.fireplayer.animations.play('leftjump');
                this.fireplayer.body.velocity.y = -205;
            }
        }
        else if (this.cursors.down.isDown)
        {
            if (this.fireplayer.body.onFloor() || this.fireplayer.body.touching.down ){
                this.fireplayer.frame = 6;
                this.fireplayer.body.velocity.x = 0;
            }
        }
        else if (this.cursors.left.isDown)
        {
            if(!this.space_key.isDown)
                this.space_down = 0; 
            this.under_pipe_exit_counter = 0;
            
            //this.player.scale.x = 1;
            this.playerdirection = "left";
            if (!this.fireplayer.body.onFloor() && !this.fireplayer.body.touching.down )
            {
                this.fireplayer.animations.stop(); 
                if(this.playerdirection== "rightjump")
                    this.fireplayer.frame = 1;
                else if(this.playerdirection == "leftjump")
                    this.fireplayer.frame = 11;
            }
            else if(this.fireplayer.body.velocity.x > -10 && this.playeridlecounter == 0 && this.playerdirection == "left" && this.fireplayer.body.velocity.x != 0) 
                this.fireplayer.frame = 12;
            else{
                this.fireplayer.animations.play('leftwalk');
                this.playeridlecounter = 0;
            }
           
            if(this.fireplayer.body.velocity.x >(-110))
                this.fireplayer.body.velocity.x -= 10;
            else
                this.fireplayer.body.velocity.x = -110;

        }
        else if (this.cursors.right.isDown)
        {
            if(!this.space_key.isDown)
                this.space_down = 0; 
            //this.player.scale.x = 1;
            this.playerdirection = "right";
            if (!this.fireplayer.body.onFloor() && !this.fireplayer.body.touching.down )
            {
                
                this.fireplayer.animations.stop(); 
                if(this.playerdirection== "rightjump")
                    this.fireplayer.frame = 1;
                else if(this.playerdirection == "leftjump")
                    this.fireplayer.frame = 11;
            }
            else if(this.fireplayer.body.velocity.x < 10  && this.playeridlecounter == 0 && this.playerdirection == "right" && this.fireplayer.body.velocity.x != 0){
            
                this.fireplayer.frame = 0;
            }
            else{
                
                this.fireplayer.animations.play('rightwalk');
                this.playeridlecounter = 0;
            }
           
            if(this.fireplayer.body.velocity.x < 110)
                this.fireplayer.body.velocity.x += 10;
            else
                this.fireplayer.body.velocity.x = 110;

        }     
        else
        {
            if(!this.space_key.isDown)
                this.space_down = 0; 
            this.playeridlecounter += 1;
           // if(this.player.body.velocity.x < 1 && this.player.body.velocity.x > 0 || this.player.body.velocity.x > -1 && this.player.body.velocity.x < 0 )
                //this.player.body.velocity.x = 0;
            if(this.fireplayer.body.velocity.x > 0)
                this.fireplayer.body.velocity.x -= this.fireplayer.body.velocity.x/2;
            else if(this.fireplayer.body.velocity.x < 0)
                this.fireplayer.body.velocity.x += this.fireplayer.body.velocity.x/2;
            
            this.fireplayer.body.velocity.x = 0;
            
            //his.player.body.velocity.x = 0;
            //this.player.loadTexture 
            this.fireplayer.animations.stop(); 
            if(this.playerdirection== "right")
                this.fireplayer.frame = 1;
            else if(this.playerdirection == "left")
                this.fireplayer.frame = 11;
        
        }
    }

    },
    hitcoinbrick: function(player,brick){
            
        if(this.who == "small"){
           this.playerOnbrick = 1;
            if(!this.player.body.touching.down && this.player.body.touching.up){ //如果player不在地板 && player頭碰到才算撞到
                if(brick.key == "question" ){  //brick.key == "question"代表還沒撞過 撞過會變成brick.key 會變成"fix"
                    //tween brick  因為brick位置有高有低 為了方便就用y區分
                    //if(brick.y < 140) 
                        //tween_brick = game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                    //else
                        //tween_brick = game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    
                    //tween_brick.onComplete.add(function(){
                        brick.loadTexture('fix', 0);       
                    //}, this);
                    this.eat_coin_sound.play();
                    game.global.score += 200;
                    game.global.coin += 1;
                    if(game.global.score < 1000)
                        this.score_text.setText("000"+game.global.score);
                    else if(game.global.score < 10000 && game.global.score >= 1000 )
                        this.score_text.setText("00"+game.global.score);
                    else if(game.global.score < 100000 && game.global.score >= 10000 )
                        this.score_text.setText("0"+game.global.score);
                    else
                        this.score_text.setText(game.global.score);

                    if(game.global.coin < 10)
                         this.money_text.setText("x"+"0"+game.global.coin);
                    else 
                        this.money_text.setText("x"+game.global.coin);
                    //console.log(game.global.score);
                    //console.log(game.global.coin);
                    coin = game.add.sprite(brick.x, brick.y-20, 'coin');
                    coin.animations.add('coin_anim', [0,1,2,3], 50, true);
               
               
                    coin.animations.play('coin_anim');
                    tween_coin = game.add.tween(coin).to({x:coin.x,y:coin.y-30},200).to({x:coin.x,y:coin.y-10},200).to({alpha: 0},10).start();
                    tween_coin.onComplete.add(function(){
                        coin.kill() ;
                        text = game.add.text(brick.x+5, brick.y-30,"200", { font: '20px VT323', fill: '#ffffff' });
                        tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                        tween_text.onComplete.add(function(){
                            text.kill() ;
                        }, this);
                    }, this);
                   
                }
            }
        }
        else if(this.who == "big"){
            this.playerOnbrick = 1;
             if(!this.bigplayer.body.touching.down && this.bigplayer.body.touching.up){ //如果player不在地板 && player頭碰到才算撞到
                 if(brick.key == "question" ){  //brick.key == "question"代表還沒撞過 撞過會變成brick.key 會變成"fix"
                     //tween brick  因為brick位置有高有低 為了方便就用y區分
                     //if(brick.y < 140) 
                         //tween_brick = game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                     //else
                         //tween_brick = game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                     
                     //tween_brick.onComplete.add(function(){
                         brick.loadTexture('fix', 0);       
                     //}, this);
                     this.eat_coin_sound.play();
                     game.global.score += 200;
                     game.global.coin += 1;
                     if(game.global.score < 1000)
                        this.score_text.setText("000"+game.global.score);
                    else if(game.global.score < 10000 && game.global.score >= 1000 )
                        this.score_text.setText("00"+game.global.score);
                    else if(game.global.score < 100000 && game.global.score >= 10000 )
                        this.score_text.setText("0"+game.global.score);
                    else
                        this.score_text.setText(game.global.score);

                     if(game.global.coin < 10)
                        this.money_text.setText("x"+"0"+game.global.coin);
                    else 
                        this.money_text.setText("x"+game.global.coin);
                     //console.log(game.global.score);
                     //console.log(game.global.coin);
                     coin = game.add.sprite(brick.x, brick.y-20, 'coin');
                     coin.animations.add('coin_anim', [0,1,2,3], 50, true);
                     coin.animations.play('coin_anim');
                     tween_coin = game.add.tween(coin).to({x:coin.x,y:coin.y-30},200).to({x:coin.x,y:coin.y-10},200).to({alpha: 0},10).start();
                     tween_coin.onComplete.add(function(){
                         coin.kill() ;
                         text = game.add.text(brick.x+5, brick.y-30,"200", { font: '20px VT323', fill: '#ffffff' });
                         tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                         tween_text.onComplete.add(function(){
                             text.kill() ;
                         }, this);
                     }, this);
                    
                 }
             }
         }
         else if(this.who == "fire"){
            this.playerOnbrick = 1;
             if(!this.fireplayer.body.touching.down && this.fireplayer.body.touching.up){ //如果player不在地板 && player頭碰到才算撞到
                 if(brick.key == "question" ){  //brick.key == "question"代表還沒撞過 撞過會變成brick.key 會變成"fix"
                     //tween brick  因為brick位置有高有低 為了方便就用y區分
                     //if(brick.y < 140) 
                         //tween_brick = game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                     //else
                         //tween_brick = game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                     
                     //tween_brick.onComplete.add(function(){
                         brick.loadTexture('fix', 0);       
                     //}, this);
                     this.eat_coin_sound.play();
                     game.global.score += 200;
                     game.global.coin += 1;
                     if(game.global.score < 1000)
                        this.score_text.setText("000"+game.global.score);
                     else if(game.global.score < 10000 && game.global.score >= 1000 )
                        this.score_text.setText("00"+game.global.score);
                     else if(game.global.score < 100000 && game.global.score >= 10000 )
                        this.score_text.setText("0"+game.global.score);
                     else
                        this.score_text.setText(game.global.score);

                     if(game.global.coin < 10)
                        this.money_text.setText("x"+"0"+game.global.coin);
                    else 
                        this.money_text.setText("x"+game.global.coin);
                     //console.log(game.global.score);
                     //console.log(game.global.coin);
                     coin = game.add.sprite(brick.x, brick.y-20, 'coin');
                     coin.animations.add('coin_anim', [0,1,2,3], 50, true);
                     coin.animations.play('coin_anim');
                     tween_coin = game.add.tween(coin).to({x:coin.x,y:coin.y-30},200).to({x:coin.x,y:coin.y-10},200).to({alpha: 0},10).start();
                     tween_coin.onComplete.add(function(){
                         coin.kill() ;
                         text = game.add.text(brick.x+5, brick.y-30,"200", { font: '20px VT323', fill: '#ffffff' });
                         tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                         tween_text.onComplete.add(function(){
                             text.kill() ;
                         }, this);
                     }, this);
                    
                 }
             }
         }
          
        },
        hitlifebrick: function(player,brick){
            
        if(this.who == "small"){
            if(!this.player.body.touching.down && this.player.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "lifebrick" ){ //brick.key == "lifebrick"代表還沒撞過 撞過會變成brick.key 會變成"fix"
        
                //game.add.tween(brick).to({x:brick.x,y:118},100).to({x:brick.x,y:128},100).start();

                brick.loadTexture('fix', 0);brick.body.checkCollision.up = true;
                //brick.body.enable = false;
                //game.time.events.add(
                    //100, // Start callback after 1000ms
                    //function(){brick.loadTexture('fix', 0);brick.body.checkCollision.up = true;}, // Callback 變成固定的磚塊
                    //this
                //);

                
                this.green_mushroom.x = brick.x;
                this.green_mushroom.y = brick.y;
                this.item_sound.play();
                tween_green_mushroom = game.add.tween(this.green_mushroom).to({x:this.green_mushroom.x,y:this.green_mushroom.y-20},1000).start();
                //this.player.scale.setTo(0.5,0.5);
                tween_green_mushroom.onComplete.add(function(){
                    game.physics.enable(this.green_mushroom);
                        this.green_mushroom.body.velocity.x = 30;
                        this.green_mushroom.body.gravity.y = 300;
                    //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                    //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                    //tween_text.onComplete.add(function(){
                        
                    //}, this);
                }, this);
                


                    
               
            }
            } 
        }
        else if(this.who == "big"){
            if(!this.bigplayer.body.touching.down && this.bigplayer.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "lifebrick" ){ //brick.key == "lifebrick"代表還沒撞過 撞過會變成brick.key 會變成"fix"
        
                //game.add.tween(brick).to({x:brick.x,y:118},100).to({x:brick.x,y:128},100).start();

                brick.loadTexture('fix', 0);brick.body.checkCollision.up = true;
                //brick.body.enable = false;
                //game.time.events.add(
                    //100, // Start callback after 1000ms
                    //function(){brick.loadTexture('fix', 0);brick.body.checkCollision.up = true;}, // Callback 變成固定的磚塊
                    //this
                //);

                
                this.green_mushroom.x = brick.x;
                this.green_mushroom.y = brick.y;
                this.item_sound.play();
                tween_green_mushroom = game.add.tween(this.green_mushroom).to({x:this.green_mushroom.x,y:this.green_mushroom.y-20},1000).start();
                //this.player.scale.setTo(0.5,0.5);
                tween_green_mushroom.onComplete.add(function(){
                    game.physics.enable(this.green_mushroom);
                        this.green_mushroom.body.velocity.x = 25;
                        this.green_mushroom.body.gravity.y = 300;
                    //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                    //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                    //tween_text.onComplete.add(function(){
                        
                    //}, this);
                }, this);
                


                    
               
            }
            } 
        }
        else if(this.who == "fire"){
            if(!this.fireplayer.body.touching.down && this.fireplayer.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "lifebrick" ){ //brick.key == "lifebrick"代表還沒撞過 撞過會變成brick.key 會變成"fix"
        
                //game.add.tween(brick).to({x:brick.x,y:118},100).to({x:brick.x,y:128},100).start();

                brick.loadTexture('fix', 0);brick.body.checkCollision.up = true;
                //brick.body.enable = false;
                //game.time.events.add(
                    //100, // Start callback after 1000ms
                    //function(){brick.loadTexture('fix', 0);brick.body.checkCollision.up = true;}, // Callback 變成固定的磚塊
                    //this
                //);

                
                this.green_mushroom.x = brick.x;
                this.green_mushroom.y = brick.y;
                this.item_sound.play();
                tween_green_mushroom = game.add.tween(this.green_mushroom).to({x:this.green_mushroom.x,y:this.green_mushroom.y-20},1000).start();
                //this.player.scale.setTo(0.5,0.5);
                tween_green_mushroom.onComplete.add(function(){
                    game.physics.enable(this.green_mushroom);
                        this.green_mushroom.body.velocity.x = 25;
                        this.green_mushroom.body.gravity.y = 300;
                    //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                    //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                    //tween_text.onComplete.add(function(){
                        
                    //}, this);
                }, this);
                


                    
               
            }
            } 
        }
            
            
            

        },
        hitstarbrick: function(player,brick){
            
        if(this.who == "small"){
            if(!this.player.body.touching.down && this.player.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "brick" ){ //brick.key == "brick"代表還沒撞過 撞過會變成brick.key 會變成"fix"
                //console.log(brick.y);
                //tween brick  因為brick位置有高有低 為了方便就用y區分
                //if(brick.y < 140)
                    //game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                //else
                    //game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    brick.loadTexture('fix', 0);
                //brick.body.enable = false;
                //game.time.events.add(
                    //100, // Start callback after 1000ms
                    //function(){brick.loadTexture('fix', 0);}, // Callback 變成固定的磚塊
                    //this
                //);

                this.star.x = brick.x;
                this.star.y = brick.y;
                this.item_sound.play();
                tween_star = game.add.tween(this.star).to({x:this.star.x,y:this.star.y-20},1000).start();

                
                //this.player.scale.setTo(0.5,0.5);
                tween_star.onComplete.add(function(){
                    
                        this.star.body.velocity.x = 40;
                        this.star.body.gravity.y = 300;
                        this.star.body.bounce.set(1);
                        
                    //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                    //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                    //tween_text.onComplete.add(function(){
                        
                    //}, this);
                }, this);
                
               
            }
            }
        }
        else if(this.who == "big"){
            if(!this.bigplayer.body.touching.down && this.bigplayer.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "brick" ){ //brick.key == "brick"代表還沒撞過 撞過會變成brick.key 會變成"fix"
                //console.log(brick.y);
                //tween brick  因為brick位置有高有低 為了方便就用y區分
                //if(brick.y < 140)
                    //game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                //else
                    //game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    brick.loadTexture('fix', 0);
                //brick.body.enable = false;
                //game.time.events.add(
                    //100, // Start callback after 1000ms
                    //function(){brick.loadTexture('fix', 0);}, // Callback 變成固定的磚塊
                    //this
                //);

                this.star.x = brick.x;
                this.star.y = brick.y;
                this.item_sound.play();
                tween_star = game.add.tween(this.star).to({x:this.star.x,y:this.star.y-20},1000).start();

                
                //this.player.scale.setTo(0.5,0.5);
                tween_star.onComplete.add(function(){
                    
                        this.star.body.velocity.x = 40;
                        this.star.body.gravity.y = 300;
                        this.star.body.bounce.set(1);
                        
                    //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                    //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                    //tween_text.onComplete.add(function(){
                        
                    //}, this);
                }, this);
                
               
            }
            }
        }
        else if(this.who == "fire"){
            if(!this.fireplayer.body.touching.down && this.fireplayer.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "brick" ){ //brick.key == "brick"代表還沒撞過 撞過會變成brick.key 會變成"fix"
                //console.log(brick.y);
                //tween brick  因為brick位置有高有低 為了方便就用y區分
                //if(brick.y < 140)
                    //game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                //else
                    //game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    brick.loadTexture('fix', 0);
                //brick.body.enable = false;
                //game.time.events.add(
                    //100, // Start callback after 1000ms
                    //function(){brick.loadTexture('fix', 0);}, // Callback 變成固定的磚塊
                    //this
                //);

                this.star.x = brick.x;
                this.star.y = brick.y;
                this.item_sound.play();
                tween_star = game.add.tween(this.star).to({x:this.star.x,y:this.star.y-20},1000).start();

                
                //this.player.scale.setTo(0.5,0.5);
                tween_star.onComplete.add(function(){
                    
                        this.star.body.velocity.x = 40;
                        this.star.body.gravity.y = 300;
                        this.star.body.bounce.set(1);
                        
                    //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                    //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                    //tween_text.onComplete.add(function(){
                        
                    //}, this);
                }, this);
                
               
            }
            }
        }
            
            
            

        },
        hitmfbrick: function(player,brick){
            
        if(this.who == "small"){
        if(!this.player.body.touching.down && this.player.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
            if(brick.key == "question" ){
                //console.log(brick.y);
                //tween brick  因為brick位置有高有低 為了方便就用y區分
                //if(brick.y < 140)
                    //game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                //else
                    //game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    this.red_mushroom.alpha = 1;
                    this.red_mushroom.x = brick.x;
                    this.red_mushroom.y = brick.y;
                    this.item_sound.play();
                    
                    tween_red_mushroom = game.add.tween(this.red_mushroom).to({x:this.red_mushroom.x,y:this.red_mushroom.y-18},1000).start();
                    //this.player.scale.setTo(0.5,0.5);
                    tween_red_mushroom.onComplete.add(function(){
                        
                        game.physics.enable(this.red_mushroom);
                        this.red_mushroom.body.enable = true;
                            this.red_mushroom.body.velocity.x = 30;
                            this.red_mushroom.body.gravity.y = 300;
                        //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                        //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                        //tween_text.onComplete.add(function(){
                            
                        //}, this);
                    }, this);
                    
                
                
                //brick.body.enable = false;
                game.time.events.add(
                    100, // Start callback after 1000ms
                    function(){brick.loadTexture('fix', 0);}, // Callback 變成固定的磚塊
                    this
                );
               
            }
        }
        }
        else if(this.who == "big"){
            if(!this.bigplayer.body.touching.down && this.bigplayer.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
                if(brick.key == "question" ){
                    //console.log(brick.y);
                    //tween brick  因為brick位置有高有低 為了方便就用y區分
                    //if(brick.y < 140)
                        //game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                    //else
                        //game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    
                        this.flower.alpha = 1;
                        this.flower.x = brick.x;
                        this.flower.y = brick.y;
                        this.flower.animations.play('anim');
                        game.physics.enable(this.flower);
                        this.flower.body.enable = true;
                        this.item_sound.play();
                        tween_flower = game.add.tween(this.flower).to({x:this.flower.x,y:this.flower.y-16},1000).start();
                        //this.player.scale.setTo(0.5,0.5);
                        /*
                        tween_flower.onComplete.add(function(){
                            game.physics.enable(this.flower);
                                this.flower.body.velocity.x = 25;
                                this.lofwer.body.gravity.y = 300;
                            //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                            //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                            //tween_text.onComplete.add(function(){
                                
                            //}, this);
                        }, this);*/
                    
                    
                    //brick.body.enable = false;
                    game.time.events.add(
                        100, // Start callback after 1000ms
                        function(){brick.loadTexture('fix', 0);}, // Callback 變成固定的磚塊
                        this
                    );
                   
                }
            }
        }
        else if(this.who == "fire"){
            if(!this.fireplayer.body.touching.down && this.fireplayer.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
                if(brick.key == "question" ){
                    //console.log(brick.y);
                    //tween brick  因為brick位置有高有低 為了方便就用y區分
                    //if(brick.y < 140)
                        //game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                    //else
                        //game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    
                        this.flower.alpha = 1;
                        this.flower.x = brick.x;
                        this.flower.y = brick.y;
                        this.flower.animations.play('anim');
                        game.physics.enable(this.flower);
                        this.flower.body.enable = true;
                        this.item_sound.play();
                        tween_flower = game.add.tween(this.flower).to({x:this.flower.x,y:this.flower.y-16},1000).start();
                        //this.player.scale.setTo(0.5,0.5);
                        /*
                        tween_flower.onComplete.add(function(){
                            game.physics.enable(this.flower);
                                this.flower.body.velocity.x = 25;
                                this.lofwer.body.gravity.y = 300;
                            //text = game.add.text(brick.x+5, brick.y-30,"200", { font: '10px Arial', fill: '#ffffff' });
                            //tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                            //tween_text.onComplete.add(function(){
                                
                            //}, this);
                        }, this);*/
                    
                    
                    //brick.body.enable = false;
                    game.time.events.add(
                        100, // Start callback after 1000ms
                        function(){brick.loadTexture('fix', 0);}, // Callback 變成固定的磚塊
                        this
                    );
                   
                }
            }
        }
            
            
            

        },
        hitbrick: function(player,brick){
            this.playerOnbrick = 1;
            //console.log(brick.key);

        if(this.who == "small"){
            if(!this.player.body.touching.down && this.player.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
                //tween brick  因為brick位置有高有低 為了方便就用y區分
                if(brick.y < 140)
                    game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                else
                    game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
            }
        }
        else{
            if(!player.body.touching.down && player.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
                //console.log("brick.x = ",brick.x);
                //console.log("brick.y = ",brick.y);
                if(brick.x == 1232)
                {
                    brick.alpha = 0;
                }
                else
                    brick.kill();
                this.break_brick = 1;
                game.time.events.add(
                    1000, // Start callback after 1000ms
                    function(){this.break_brick = 0;}, // Callback 變成固定的磚塊
                    this
                );
                this.brick_sound.play();
                this.emitter.x = brick.x+8;
                this.emitter.y = brick.y;
                this.emitter.start(true, 400, null, 1); 
                this.emitter2.x = brick.x+8;
                this.emitter2.y = brick.y;
                this.emitter2.start(true, 400, null, 1); 
                this.emitter3.x = brick.x+8;
                this.emitter3.y = brick.y;
                this.emitter3.start(true, 400, null, 1); 
                this.emitter4.x = brick.x+8;
                this.emitter4.y = brick.y;
                this.emitter4.start(true, 400, null, 1); 
            }
        }
           
            
            
            

        },
        hitctbrick: function(player,brick){
            
            //console.log(brick.key);
        if(this.who == "small"){
            if(!this.player.body.touching.down  && this.player.body.touching.up){//如果player不在地板 && player頭碰到才算撞到
                
                if(brick.key != "fix"){
                    if(brick.y < 140)
                        game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                    else
                        game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    this.continue_coin_count -= 1;
                    console.log("continue_count = " + this.continue_coin_count);

                    this.eat_coin_sound.play();
                    game.global.score += 200;
                    game.global.coin += 1;
                    if(game.global.score < 1000)
                        this.score_text.setText("000"+game.global.score);
                    else if(game.global.score < 10000 && game.global.score >= 1000 )
                        this.score_text.setText("00"+game.global.score);
                    else if(game.global.score < 100000 && game.global.score >= 10000 )
                        this.score_text.setText("0"+game.global.score);
                    else
                        this.score_text.setText(game.global.score);

                    if(game.global.coin < 10)
                        this.money_text.setText("x"+"0"+game.global.coin);
                    else 
                        this.money_text.setText("x"+game.global.coin);
                    //console.log("score =  " + game.global.score);
                    //console.log("coin = " + game.global.coin);
                    coin = game.add.sprite(brick.x, brick.y-20, 'coin');
                    coin.animations.add('coin_anim', [0,1,2,3], 50, true);
                    coin.animations.play('coin_anim');
                    tween_coin = game.add.tween(coin).to({x:coin.x,y:coin.y-30},200).to({x:coin.x,y:coin.y-10},200).to({alpha: 0},10).start();
                    tween_coin.onComplete.add(function(){
                        coin.kill() ;
                        text = game.add.text(brick.x+5, brick.y-10,"200", { font: '20px VT323', fill: '#ffffff' });
                        tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                        tween_text.onComplete.add(function(){
                            text.kill() ;
                        }, this);
                    }, this);

                    if(this.continue_coin_count == 0)
                        brick.loadTexture('fix', 0);
                }
                


            }
        }
        else if(this.who == "big"){
            if(!this.bigplayer.body.touching.down  && this.bigplayer.body.touching.up && this.tween_ct_brick == 0){//如果player不在地板 && player頭碰到才算撞到
                
                if(brick.key != "fix"){
                    this.tween_ct_brick = 1;
                    if(brick.y < 140)
                        tweenctbrick = game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                    else
                        tweenctbrick =game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    tweenctbrick.onComplete.add(function(){
                        this.tween_ct_brick = 0;
                    }, this);
                    this.continue_coin_count -= 1;
                    console.log("continue_count = " + this.continue_coin_count);

                    this.eat_coin_sound.play();
                    game.global.score += 200;
                    game.global.coin += 1;
                    if(game.global.score < 1000)
                        this.score_text.setText("000"+game.global.score);
                    else if(game.global.score < 10000 && game.global.score >= 1000 )
                        this.score_text.setText("00"+game.global.score);
                    else if(game.global.score < 100000 && game.global.score >= 10000 )
                        this.score_text.setText("0"+game.global.score);
                    else
                        this.score_text.setText(game.global.score);

                    if(game.global.coin < 10)
                        this.money_text.setText("x"+"0"+game.global.coin);
                    else 
                        this.money_text.setText("x"+game.global.coin);
                    //console.log("score =  " + game.global.score);
                    //console.log("coin = " + game.global.coin);
                    coin = game.add.sprite(brick.x, brick.y-20, 'coin');
                    coin.animations.add('coin_anim', [0,1,2,3], 50, true);
                    coin.animations.play('coin_anim');
                    tween_coin = game.add.tween(coin).to({x:coin.x,y:coin.y-30},200).to({x:coin.x,y:coin.y-10},200).to({alpha: 0},10).start();
                    tween_coin.onComplete.add(function(){
                        coin.kill() ;
                        text = game.add.text(brick.x+5, brick.y-10,"200", { font: '20px VT323', fill: '#ffffff' });
                        tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                        tween_text.onComplete.add(function(){
                            text.kill() ;
                        }, this);
                    }, this);

                    if(this.continue_coin_count == 0)
                        brick.loadTexture('fix', 0);
                }
                

            }
        }
        else if(this.who == "fire"){
            if(!this.fireplayer.body.touching.down  && this.fireplayer.body.touching.up && this.tween_ct_brick == 0){//如果player不在地板 && player頭碰到才算撞到
                
                if(brick.key != "fix"){
                    this.tween_ct_brick = 1;
                    if(brick.y < 140)
                        tweenctbrick = game.add.tween(brick).to({x:brick.x,y:70},100).to({x:brick.x,y:80},100).start();
                    else
                        tweenctbrick =game.add.tween(brick).to({x:brick.x,y:134},100).to({x:brick.x,y:144},100).start();
                    tweenctbrick.onComplete.add(function(){
                        this.tween_ct_brick = 0;
                    }, this);
                    this.continue_coin_count -= 1;
                    console.log("continue_count = " + this.continue_coin_count);

                    this.eat_coin_sound.play();
                    game.global.score += 200;
                    game.global.coin += 1;
                    if(game.global.score < 1000)
                        this.score_text.setText("000"+game.global.score);
                    else if(game.global.score < 10000 && game.global.score >= 1000 )
                        this.score_text.setText("00"+game.global.score);
                    else if(game.global.score < 100000 && game.global.score >= 10000 )
                        this.score_text.setText("0"+game.global.score);
                    else
                        this.score_text.setText(game.global.score);

                    if(game.global.coin < 10)
                        this.money_text.setText("x"+"0"+game.global.coin);
                    else 
                        this.money_text.setText("x"+game.global.coin);
                    //console.log("score =  " + game.global.score);
                    //console.log("coin = " + game.global.coin);
                    coin = game.add.sprite(brick.x, brick.y-20, 'coin');
                    coin.animations.add('coin_anim', [0,1,2,3], 50, true);
                    coin.animations.play('coin_anim');
                    tween_coin = game.add.tween(coin).to({x:coin.x,y:coin.y-30},200).to({x:coin.x,y:coin.y-10},200).to({alpha: 0},10).start();
                    tween_coin.onComplete.add(function(){
                        coin.kill() ;
                        text = game.add.text(brick.x+5, brick.y-10,"200", { font: '20px VT323', fill: '#ffffff' });
                        tween_text = game.add.tween(text).to({x:text.x,y:text.y-30},600).to({alpha: 0},10).start();
                        tween_text.onComplete.add(function(){
                            text.kill() ;
                        }, this);
                    }, this);

                    if(this.continue_coin_count == 0)
                        brick.loadTexture('fix', 0);
                }
                

            }
        }
    }
      
    
    

};



function random_velocity(items)
{
     return items[Math.floor(Math.random()*2)];
}
